package main

/*
   #include <stdlib.h>

   struct Data {
    int size;
    void* data;
    };

   struct IntAndError {
    int error;
    int val;
    };

   struct DataAndError {
    int error;
    struct Data data;
    };

   struct Reference {
    int error;
    long reference;
    };

   struct Round1Data {
    int error;
    struct Data unicastLengths;
    struct Data unicastData;
    struct Data broadcastData;
    };

   struct Round7Data {
    int error;
    char* keyId;
    struct Data preSigIds;
    struct Data honestPlayers;
    };

   struct CombinedSig {
    int error;
    struct Data signature;
    struct Data contributors;
    };

*/
import "C"

import (
    "fmt"
    "gitlab.com/sepior/pbcts-lib/player"
    "gitlab.com/sepior/pbcts-lib/signature"
    "log"
    "math/rand"
    "strings"
    "sync"
    "unsafe"
)

var players sync.Map
var sessions sync.Map

// generateReference generates a new externally "trackable reference". An
// externally trackable reference is a random long.
func generateReference() int64 {
    return rand.Int63()
}

// createPlayerReference creates a new externally trackable reference for a
// player object.
func createPlayerReference() int64 {
    ref := generateReference()
    // try again if the generated reference is already in use.
    if _, ok := players.Load(ref); ok {
        return createPlayerReference()
    }
    return ref
}

// createSessionReference creates a new externally trackable reference for a
// key-gen session object.
func createSessionReference() int64 {
    ref := generateReference()
    // try again if the generated reference is already in use.
    if _, ok := sessions.Load(ref); ok {
        return createSessionReference()
    }
    return ref
}

// createCBool creates a C boolean. I.e., an int which is 0 if v is false and 1
// if v is true.
func createCBool(v bool) C.int {
    if v {
        return C.int(1)
    }
    return C.int(0)
}

// nay creates a false C boolean.
func nay() C.int {
    return createCBool(true)
}

// yay creates a true C boolean.
func yay() C.int {
    return createCBool(false)
}

// createDummyData creates a Data struct with a value that is not intended to be
// read. This is the case when an externally callable function would return an
// error. In that case, the function would return a boolean indicating that an
// error happened together with some dummy data (that's not supposed to be
// read).
func createDummyData() C.struct_Data {
    return C.struct_Data{C.int(0), nil}
}

// createError creates an error suitable for a function returning a DataAndError
// struct.
func createError() C.struct_DataAndError {
    return C.struct_DataAndError{nay(), createDummyData()}
}

// dataFromBytes converts a Go byte array to an equivalent C struct.
func dataFromBytes(data []byte) C.struct_Data {
    return C.struct_Data{C.int(len(data)), C.CBytes(data)}
}

// pbctsNew takes a filename and backup filename, and a database encryption key
// and returns a new player reference that can be tracked
// externally. databaseFilename cannot point to an existing file. If
// databaseFilename already exists, there's a chance PbctsLoad should be used
// instead.
//export pbctsNew
func pbctsNew(databaseFilename, databaseBackupFilename string, databaseKey []byte) C.struct_Reference {
    p, err := player.New(databaseFilename, databaseBackupFilename, databaseKey)
    if err != nil {
        log.Printf("error while creating player: %s", err)
        return C.struct_Reference{nay(), 0}
    } else {
        ref := createPlayerReference()
        players.Store(ref, p)
        return C.struct_Reference{yay(), C.long(ref)}
    }
}

// pbctsLoad loads an existing player from a database and returns an externally
// trackable reference to it. databaseFilename should point to the database
// where the player object is stored.
//export pbctsLoad
func pbctsLoad(databaseFilename, databaseBackupFilename string, databaseKey []byte) C.struct_Reference {
    p, err := player.Load(databaseFilename, databaseBackupFilename, databaseKey)
    if err != nil {
        log.Printf("error while loading player: %s", err)
        return C.struct_Reference{nay(), 0}
    } else {
        ref := createPlayerReference()
        players.Store(ref, p)
        return C.struct_Reference{yay(), C.long(ref)}
    }
}

// getPlayer takes an externally trackable player reference and returns the
// corresponding player object.
func getPlayer(playerReference int64) (*player.Player, error) {
    p, ok := players.Load(playerReference)
    if !ok {
        return nil, fmt.Errorf("no player with reference: %d", playerReference)
    }
    return p.(*player.Player), nil
}

// pbctsNewKeyGenSession takes an player reference, player identifier ID, player
// count N, pre-signature count M, and session ID, and creates a new externally
// trackable reference for a key generation protocol for N players that will
// generate M pre-signatures, and where this player has the id ID.
//export pbctsNewKeyGenSession
func pbctsNewKeyGenSession(playerReference int64, playerIndex, playerCount, preSignatureCount int, sessionId string) C.struct_Reference {
    sessionIdCopy := strings.Clone(sessionId)
    p, err := getPlayer(playerReference)
    if err != nil {
        fmt.Printf("%s\n", err)
        return C.struct_Reference{nay(), C.long(0)}
    }

    s, err := p.NewKeyGenSession(sessionIdCopy, playerIndex, playerCount, preSignatureCount)
    if err != nil {
        fmt.Printf("could not create a new key-gen session: %s\n", err)
        return C.struct_Reference{nay(), C.long(0)}
    }

    ref := createSessionReference()
    sessions.Store(ref, s)
    return C.struct_Reference{yay(), C.long(ref)}

}

// pbctsResumeKeyGenSession attempts to resume a key-gen session from a provided
// session ID.
//export pbctsResumeKeyGenSession
func pbctsResumeKeyGenSession(playerReference int64, sessionId string) C.struct_Reference {
    sessionIdCopy := strings.Clone(sessionId)
    p, err := getPlayer(playerReference)
    if err != nil {
        fmt.Printf("%s\n", err)
        return C.struct_Reference{nay(), C.long(0)}
    }
    s, err := p.ResumeKeyGenSession(sessionIdCopy)
    if err != nil {
        fmt.Printf("%s\n", err)
        return C.struct_Reference{nay(), C.long(0)}
    }

    ref := createSessionReference()
    sessions.Store(ref, s)
    return C.struct_Reference{yay(), C.long(ref)}
}

// pbctsFlushKeyGenSessionCache flushes all sessions that are held in memory. Existing sessions are
// invalidated after calling this function.
//export pbctsFlushKeyGenSessionCache
func pbctsFlushKeyGenSessionCache(playerReference int64) bool {
    p, err := getPlayer(playerReference)
    if err != nil {
        fmt.Printf("#{err}\n")
        return false
    }
    p.FlushKeyGenSessionCache()
    return true
}

// pbctsPublicKey takes a player reference, key ID and returns the public key
// associated with the key ID.
//export pbctsPublicKey
func pbctsPublicKey(playerReference int64, keyId string) C.struct_DataAndError {
    p, err := getPlayer(playerReference)
    if err != nil {
        fmt.Printf("%s\n", err)
        return createError()
    }

    pk, err := p.PublicKey(keyId)
    if err != nil {
        fmt.Printf("could not find public-key: %s\n", err)
        return createError()
    }

    return C.struct_DataAndError{yay(), dataFromBytes(pk)}
}

// pbctsSign takes a player reference, public key ID, pre-signature ID, message
// hash and pre-signature randomizer, and partially signs the provided
// message. The pre-signature ID specifies which pre-signature to use and the
// pre-signature randomizer is used to re-randomize the pre-signature before
// usage.
//export pbctsSign
func pbctsSign(playerReference int64, keyId, preSignatureId string, message []byte, preSignatureRandomizer []byte) C.struct_DataAndError {
    p, err := getPlayer(playerReference)
    if err != nil {
        fmt.Printf("%s\n", err)
        return createError()
    }

    partialSignature, err := p.Sign(keyId, preSignatureId, message, preSignatureRandomizer)
    if err != nil {
        fmt.Printf("%s\n", err)
        return createError()
    }

    return C.struct_DataAndError{yay(), dataFromBytes(partialSignature)}
}

// pbctsCombine takes a list of partial signatures and returns a full ECDSA
// signature (r, s) where each of r and s are 32 byte big-endian values.
//export pbctsCombine
func pbctsCombine(partialSignatures [][]byte) C.struct_CombinedSig {
    s, c, err := signature.Combine(partialSignatures)
    if err != nil {
        fmt.Printf("%s\n", err)
        return C.struct_CombinedSig{nay(), createDummyData(), createDummyData()}
    }

    return C.struct_CombinedSig{yay(), dataFromBytes(s), dataFromInts(c)}
}

// pbctsDeleteKeyShare deletes an existing key share from the database.
//export pbctsDeleteKeyShare
func pbctsDeleteKeyShare(playerReference int64, keyId string) bool {
    p, err := getPlayer(playerReference)
    if err != nil {
        fmt.Printf("%s\n", err)
        return false
    }

    err = p.DeleteKeyShare(keyId)
    if err != nil {
        fmt.Printf("%s\n", err)
        return false
    }

    return true
}

// pbctsDeletePreSignature deletes the data associated with a particular
// pre-signature ID.
//export pbctsDeletePreSignature
func pbctsDeletePreSignature(playerReference int64, keyId, preSignatureId string) bool {
    p, err := getPlayer(playerReference)
    if err != nil {
        fmt.Printf("%s\n", err)
        return false
    }

    err = p.DeletePresignature(keyId, preSignatureId)
    if err != nil {
        fmt.Printf("%s\n", err)
        return false
    }

    return true
}

// pbctsDeleteAllPreSignatures delete all pre-signature data associated with a
// particular key ID.
//export pbctsDeleteAllPreSignatures
func pbctsDeleteAllPreSignatures(playerReference int64, keyId string) bool {
    p, err := getPlayer(playerReference)
    if err != nil {
        fmt.Printf("%s\n", err)
        return false
    }

    err = p.DeleteAllPresignatures(keyId)
    if err != nil {
        fmt.Printf("%s\n", err)
        return false
    }

    return true
}

// pbctsPreSignatureCount count the number of available pre-signatures for a
// given key ID.
//export pbctsPreSignatureCount
func pbctsPreSignatureCount(playerReference int64, keyId string) C.struct_IntAndError {
    p, err := getPlayer(playerReference)
    if err != nil {
        fmt.Printf("%s\n", err)
        return C.struct_IntAndError{nay(), C.int(0)}
    }

    count, err := p.PresignatureCount(keyId)
    if err != nil {
        fmt.Printf("%s\n", err)
        return C.struct_IntAndError{nay(), C.int(0)}
    }

    return C.struct_IntAndError{yay(), C.int(count)}
}

// dataFromStrings converts an array of strings into a data struct. The strings
// are placed sequentially and will be terminated with a \0 byte. This method
// allocates memory that must be freed by the caller.
func dataFromStrings(strings []string) C.struct_Data {
    n := len(strings)
    totalSize := 0
    for i := 0; i < n; i++ {
        totalSize += len(strings[i])
    }

    // allocate size for all the strings + terminating null bytes
    ptr := C.malloc(C.size_t(totalSize + n))
    // "trick" to convert the above ptr to a Go array
    stringsArray := (*[1<<30 - 1]C.char)(ptr)[:(totalSize + n):(totalSize + n)]
    idx := 0
    for i := 0; i < n; i++ {
        str := strings[i]
        for j := 0; j < len(str); j++ {
            stringsArray[idx] = C.char(str[j])
            idx++
        }
        // null byte so we know when this string ended
        stringsArray[idx] = C.char(0)
        idx++
    }

    return C.struct_Data{C.int(n), unsafe.Pointer((*C.char)(ptr))}
}

// pbctsListPreSignatureIds lists all the pre-signature IDs associated with a
// key ID. This function allocates memory that must be freed by the caller.
//export pbctsListPreSignatureIds
func pbctsListPreSignatureIds(playerReference int64, keyId string) C.struct_DataAndError {
    p, err := getPlayer(playerReference)
    if err != nil {
        fmt.Printf("%s\n", err)
        return createError()
    }

    ids, err := p.ListPresignatureIDs(keyId)
    if err != nil {
        fmt.Printf("%s\n", err)
        return createError()
    }

    return C.struct_DataAndError{yay(), dataFromStrings(ids)}
}

func getSession(sessionReference int64) (*player.KeyGenSession, error) {
    s, ok := sessions.Load(sessionReference)
    if !ok {
        return nil, fmt.Errorf("no session with reference: %d", sessionReference)
    }
    return s.(*player.KeyGenSession), nil
}

// round1Error returns a piece of data that indicates that round 1 of the
// protocol resulted in an error.
func round1Error() C.struct_Round1Data {
    return C.struct_Round1Data{nay(), createDummyData(), createDummyData(), createDummyData()}
}

// flattenArrays flattens a 2d byte array and returns the flattened array
// together with an array of the lengths of the flattened arrays.
func flattenArrays(data [][]byte) ([]int, []byte) {
    f := make([]byte, 0, len(data))
    s := make([]int, 0, len(data))
    for i := 0; i < len(data); i++ {
        f = append(f, data[i]...)
        s = append(s, len(data[i]))
    }
    return s, f
}

// dataFromInts converts an array of ints to a C data struct.
func dataFromInts(ints []int) C.struct_Data {
    n := len(ints)
    ptr := C.malloc(C.size_t(n) * C.size_t(unsafe.Sizeof(C.long(0))))
    intArray := (*[1<<30 - 1]C.long)(ptr)[:n:n]
    for i, v := range ints {
        intArray[i] = C.long(v)
    }
    return C.struct_Data{C.int(n), unsafe.Pointer((*C.long)(ptr))}
}

// pbctsRound1 runs round 1 of the key generation protocol.
//export pbctsRound1
func pbctsRound1(sessionReference int64) C.struct_Round1Data {
    s, err := getSession(sessionReference)
    if err != nil {
        fmt.Printf("%s\n", err)
        return round1Error()
    }

    unicast, broadcast, err := s.Round1()
    if err != nil {
        fmt.Printf("error round1: %s\n", err)
        return round1Error()
    }

    unicastLens, unicastBytes := flattenArrays(unicast)
    return C.struct_Round1Data{
        yay(),
        dataFromInts(unicastLens),
        dataFromBytes(unicastBytes),
        dataFromBytes(broadcast)}
}

// pbctsRound2 runs round 2 of the key generation protocol.
//export pbctsRound2
func pbctsRound2(sessionReference int64, round1UnicastMessages, round1BroadcastMessages [][]byte) C.struct_DataAndError {
    s, err := getSession(sessionReference)
    if err != nil {
        fmt.Printf("%s\n", err)
        return createError()
    }

    bm, err := s.Round2(round1UnicastMessages, round1BroadcastMessages)
    if err != nil {
        fmt.Printf("round2: %s\n", err)
        return createError()
    }

    return C.struct_DataAndError{yay(), dataFromBytes(bm)}
}

// pbctsRound3 runs round 3 of the key generation protocol.
//export pbctsRound3
func pbctsRound3(sessionReference int64, round2BroadcastMessages [][]byte) C.struct_DataAndError {
    s, err := getSession(sessionReference)
    if err != nil {
        fmt.Printf("%s\n", err)
        return createError()
    }

    bm, err := s.Round3(round2BroadcastMessages)
    if err != nil {
        fmt.Printf("round3: %s\n", err)
        return createError()
    }

    return C.struct_DataAndError{yay(), dataFromBytes(bm)}
}

// pbctsRound4 runs round 4 of the key generation protocol.
//export pbctsRound4
func pbctsRound4(sessionReference int64, round3BroadcastMessages [][]byte) C.struct_DataAndError {
    s, err := getSession(sessionReference)
    if err != nil {
        fmt.Printf("%s\n", err)
        return createError()
    }

    bm, err := s.Round4(round3BroadcastMessages)
    if err != nil {
        fmt.Printf("round4: %s\n", err)
        return createError()
    }

    return C.struct_DataAndError{yay(), dataFromBytes(bm)}
}

// pbctsRound5 runs round 5 of the key generation protocol.
//export pbctsRound5
func pbctsRound5(sessionReference int64, round4BroadcastMessages [][]byte) C.struct_DataAndError {
    s, err := getSession(sessionReference)
    if err != nil {
        fmt.Printf("%s\n", err)
        return createError()
    }

    bm, err := s.Round5(round4BroadcastMessages)
    if err != nil {
        fmt.Printf("round5: %s\n", err)
        return createError()
    }

    return C.struct_DataAndError{yay(), dataFromBytes(bm)}
}

// pbctsRound6 runs round 6 of the key generation protocol.
//export pbctsRound6
func pbctsRound6(sessionReference int64, round5BroadcastMessages [][]byte) C.struct_DataAndError {
    s, err := getSession(sessionReference)
    if err != nil {
        fmt.Printf("%s\n", err)
        return createError()
    }

    bm, err := s.Round6(round5BroadcastMessages)
    if err != nil {
        fmt.Printf("round6: %s\n", err)
        return createError()
    }

    return C.struct_DataAndError{yay(), dataFromBytes(bm)}
}

// round7Error returns a piece of data signaling that round 7 of the protocol
// had an error.
func round7Error() C.struct_Round7Data {
    return C.struct_Round7Data{nay(), nil, createDummyData(), createDummyData()}
}

// pbctsRound7 runs the 7'th and final round of the key generation protocol.
//export pbctsRound7
func pbctsRound7(sessionReference int64, round6BroadcastMessages [][]byte) C.struct_Round7Data {
    s, err := getSession(sessionReference)
    if err != nil {
        fmt.Printf("%s\n", err)
        return round7Error()
    }

    keyId, preSigIds, honestPlayers, err := s.Round7(round6BroadcastMessages)
    if err != nil {
        fmt.Printf("round 7: %s\n", err)
        return round7Error()
    }

    return C.struct_Round7Data{
        yay(),
        C.CString(keyId),
        dataFromStrings(preSigIds),
        dataFromInts(honestPlayers),
    }
}

// pbctsClose closes the database object held by a player.
//export pbctsClose
func pbctsClose(playerReference int64) bool {
    p, err := getPlayer(playerReference)
    if err != nil {
        fmt.Printf("%s\n", err)
        return false
    }

    err = p.Close()
    if err != nil {
        fmt.Printf("%s\n", err)
        return false
    }

    players.Delete(playerReference)
    return true
}

// pbctsDeleteSession deletes a session given its associated reference
//export pbctsDeleteSession
func pbctsDeleteSession(sessionReference int64) bool {
    _, err := getSession(sessionReference)
    if err != nil {
        fmt.Printf("%s\n", err)
        return false
    }

    sessions.Delete(sessionReference)
    return true
}

// required in order to compile to a shared library.
func main() {}
