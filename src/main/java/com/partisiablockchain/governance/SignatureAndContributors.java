package com.partisiablockchain.governance;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Signature;
import java.util.List;

/**
 * Container for a signature and a list of public keys of signers who contributed to creating it.
 */
public final class SignatureAndContributors {

  /** Signature. */
  public Signature signature;

  /** The signature ID used. */
  public String signatureId;

  /** Contributors. */
  public List<Long> contributors;

  /**
   * Default constructor.
   *
   * @param signature the signature
   * @param signatureId the signature ID
   * @param contributors the contributors
   */
  public SignatureAndContributors(
      Signature signature, String signatureId, List<Long> contributors) {
    this.signature = signature;
    this.signatureId = signatureId;
    this.contributors = contributors;
  }
}
