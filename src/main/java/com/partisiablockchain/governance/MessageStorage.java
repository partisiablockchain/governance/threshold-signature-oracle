package com.partisiablockchain.governance;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;

/**
 * Storage for protocol messages and partial signature data. This interface is used to pass messages
 * between a ThresholdKeyGenProtocol, or ThresholdSignatureProvider (both of which will generate
 * messages that needs to be available for other protocol participants) and ThresholdResource
 * (through which these messages are accessed).
 */
public interface MessageStorage {

  /**
   * Get a partial signature.
   *
   * @param sessionId the session id
   * @param signatureId the signature id
   * @return partial signature data.
   */
  byte[] getPartialSignature(ThresholdSessionId sessionId, String signatureId);

  /**
   * Add a partial signature to the storage.
   *
   * @param sessionId the session id
   * @param signatureId the signature id
   * @param data the data
   */
  void setPartialSignature(ThresholdSessionId sessionId, String signatureId, byte[] data);

  /**
   * Get a protocol round message intended for some party.
   *
   * @param sessionId the session id
   * @param roundNumber the round number
   * @param to the receiver of the message
   * @return round message data.
   */
  byte[] getRoundUnicastMessage(
      ThresholdSessionId sessionId, int roundNumber, BlockchainPublicKey to);

  /**
   * Add a round message to the storage.
   *
   * @param sessionId the session id
   * @param roundNumber the round number
   * @param to the receiver
   * @param data the data
   */
  void setRoundUnicastMessage(
      ThresholdSessionId sessionId, int roundNumber, BlockchainPublicKey to, byte[] data);

  /**
   * Get a broadcast message.
   *
   * @param messageHash the hash of the broadcast message
   * @return a broadcast message.
   */
  byte[] getBroadcastMessage(Hash messageHash);

  /**
   * Set the broadcast message.
   *
   * @param sessionId the session ID that the message belongs to
   * @param roundNumber the round that the message belongs to
   * @param message the message
   */
  void setBroadcastMessage(ThresholdSessionId sessionId, int roundNumber, byte[] message);
}
