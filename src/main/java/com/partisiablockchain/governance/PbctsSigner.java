package com.partisiablockchain.governance;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.governance.pbcts.PbctsProxy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** A threshold signer based on pbcts-lib. */
public final class PbctsSigner implements ThresholdSignatureProvider {

  private static final Logger logger = LoggerFactory.getLogger(PbctsSigner.class);

  private final String keyId;
  private final ThresholdSessionId sessionId;
  private final List<String> signatureIds;
  private final List<BlockchainPublicKey> signers;
  private final BlockchainPublicKey me;
  private final MessageStorage storage;
  private final List<Long> honestIndices;
  private final PbctsProxy proxy;

  private final Map<String, Hash> usedSignatureIds;
  private final Map<String, Map<BlockchainPublicKey, byte[]>> inProgress;

  /**
   * Load a {@link PbctsSigner} from a database file.
   *
   * @param sessionId the session ID
   * @param publicKeyId the public key ID associated with the key share we wish to use
   * @param signers a list of signers
   * @param me address of this party
   * @param storage a storage object for storing partial signatures
   * @param honestIndices indices of the honest parties from the key generation
   * @param databaseName filename of the database
   * @param databaseBackupName filename of the backup database
   * @param databaseKey encryption key for the database
   * @return a signer object.
   */
  public static PbctsSigner load(
      ThresholdSessionId sessionId,
      String publicKeyId,
      List<BlockchainPublicKey> signers,
      BlockchainPublicKey me,
      MessageStorage storage,
      List<Long> honestIndices,
      String databaseName,
      String databaseBackupName,
      byte[] databaseKey) {
    PbctsProxy proxy = PbctsProxy.load(databaseName, databaseBackupName, databaseKey);
    return new PbctsSigner(
        publicKeyId,
        sessionId,
        proxy.getPreSignatureIds(publicKeyId),
        signers,
        me,
        storage,
        honestIndices,
        proxy);
  }

  PbctsSigner(
      String keyId,
      ThresholdSessionId sessionId,
      List<String> signatureIds,
      List<BlockchainPublicKey> signers,
      BlockchainPublicKey me,
      MessageStorage storage,
      List<Long> honestIndices,
      PbctsProxy proxy) {
    this.keyId = keyId;
    this.sessionId = sessionId;
    this.signatureIds = signatureIds;
    this.signers = signers;
    this.me = me;
    this.storage = storage;
    this.honestIndices = honestIndices;
    this.proxy = proxy;

    this.usedSignatureIds = new HashMap<>();
    this.inProgress = new HashMap<>();
  }

  @Override
  public ThresholdPublicKey getPublicKey() {
    BlockchainPublicKey publicKey = proxy.getPublicKey(keyId);
    return new ThresholdPublicKey(publicKey, keyId);
  }

  @Override
  public int getRemainingSignatures() {
    return signatureIds.size() - usedSignatureIds.size();
  }

  @Override
  public List<String> getSignatureIds() {
    return signatureIds;
  }

  @Override
  public void preSignAndStore(Hash message, String signatureId, byte[] preSignatureRandomizer) {
    if (!signatureIds.contains(signatureId)) {
      throw new IllegalArgumentException("Unknown signature ID: " + signatureId);
    }

    usedSignatureIds.put(signatureId, message);
    byte[] partialSignature = proxy.sign(keyId, signatureId, message, preSignatureRandomizer);
    storePartialSignature(signatureId, partialSignature);
  }

  @Override
  public List<SignatureAndContributors> collectNewSignatures(PartialSignatureRetriever retriever) {
    List<SignatureAndContributors> finalizedSignatures = new ArrayList<>();
    for (String signatureId : usedSignatureIds.keySet()) {
      Map<BlockchainPublicKey, byte[]> partialSignatures = inProgress.get(signatureId);
      if (partialSignatures != null) {
        retrievePartialSignatures(signatureId, partialSignatures, retriever);

        int messageCount = 0;
        List<byte[]> sorted = new ArrayList<>();
        for (BlockchainPublicKey signer : signers) {
          byte[] partialSignature = partialSignatures.get(signer);
          if (partialSignature != null) {
            messageCount++;
            sorted.add(partialSignature);
          } else {
            sorted.add(new byte[0]);
          }
        }

        if (messageCount >= combinationThreshold()) {
          Hash message = usedSignatureIds.get(signatureId);
          SignatureAndContributors sac = attemptCombination(message, signatureId, sorted);
          if (sac != null) {
            logger.info(
                "Created signature on message=" + message + ", contributors=" + sac.contributors);
            finalizedSignatures.add(sac);
            inProgress.remove(signatureId);
          }
        }
      }
    }
    return finalizedSignatures;
  }

  private SignatureAndContributors attemptCombination(
      Hash message, String signatureId, List<byte[]> sorted) {
    try {
      PbctsProxy.CombinedSignature combined =
          proxy.combine(sorted, message, getPublicKey().getPublicKey());
      return new SignatureAndContributors(combined.signature, signatureId, combined.contributors);
    } catch (SignatureCombiningException exception) {
      logger.info("Error combining signature: " + exception);
      return null;
    }
  }

  int combinationThreshold() {
    int threshold = (signers.size() - 1) / 3;
    return 2 * threshold + 1;
  }

  private void retrievePartialSignatures(
      String signatureId,
      Map<BlockchainPublicKey, byte[]> partialSignatures,
      PartialSignatureRetriever retriever) {
    for (BlockchainPublicKey signer : getHonestParties()) {
      partialSignatures.computeIfAbsent(
          signer, s -> retriever.retrievePartialSignature(sessionId, signatureId, s));
    }
  }

  @Override
  public List<BlockchainPublicKey> getHonestParties() {
    return honestIndices.stream().map(idx -> signers.get(idx.intValue())).toList();
  }

  private void storePartialSignature(String signatureId, byte[] partialSignature) {
    Map<BlockchainPublicKey, byte[]> partialSignatureData = new HashMap<>();
    partialSignatureData.put(me, partialSignature);
    inProgress.put(signatureId, partialSignatureData);
    storage.setPartialSignature(sessionId, signatureId, partialSignature);
  }
}
