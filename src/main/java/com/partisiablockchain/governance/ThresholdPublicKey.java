package com.partisiablockchain.governance;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.BlockchainPublicKey;

/** A threshold public key. */
public final class ThresholdPublicKey {

  private final BlockchainPublicKey publicKey;
  private final String publicKeyId;

  /**
   * Create a new threshold public key object.
   *
   * @param publicKey a public key
   * @param publicKeyId the ID of the public key
   */
  public ThresholdPublicKey(BlockchainPublicKey publicKey, String publicKeyId) {
    this.publicKey = publicKey;
    this.publicKeyId = publicKeyId;
  }

  /**
   * The actual public key.
   *
   * @return the public key.
   */
  public BlockchainPublicKey getPublicKey() {
    return publicKey;
  }

  /**
   * Get the ID of this public key.
   *
   * @return the ID of this key.
   */
  public String getPublicKeyId() {
    return publicKeyId;
  }

  @Override
  public String toString() {
    return "ThresholdPublicKey{"
        + "publicKey="
        + publicKey
        + ", publicKeyId='"
        + publicKeyId
        + '\''
        + '}';
  }
}
