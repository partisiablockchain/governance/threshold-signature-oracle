package com.partisiablockchain.governance.pbcts;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.sun.jna.Memory;
import com.sun.jna.Pointer;
import com.sun.jna.Structure;
import java.util.List;

/**
 * Definitions of types used by JNA to execute code which was compiled from Go. The corresponding C
 * types can be found in the file pbcts.h.
 */
final class PbctsTypes {

  private PbctsTypes() {}

  /**
   * Convert a Java string to a GoString that can be passed by value.
   *
   * @param string the string to convert
   * @return a GoString.
   */
  static GoString.ByValue stringToGoString(String string) {
    PbctsTypes.GoString.ByValue str = new PbctsTypes.GoString.ByValue();
    int stringSize = string.length();
    str.size = stringSize;
    // This assumes string characters occupy one byte, which may not be true on all systems.
    str.data = new Memory(stringSize + 1);
    // this also sets the byte at stringSize + 1 to 0.
    str.data.setString(0, string);
    return str;
  }

  /**
   * Convert a Java byte array to a GoSlice that can be passed by value.
   *
   * @param bytes the byte array to convert
   * @return a GoSlice.
   */
  static PbctsTypes.GoSlice.ByValue bytesToGoSlice(byte[] bytes) {
    int n = bytes.length;
    PbctsTypes.GoSlice.ByValue slice = new PbctsTypes.GoSlice.ByValue();
    slice.len = n;
    slice.cap = n;
    slice.data = new Memory(n);
    slice.data.write(0, bytes, 0, n);
    return slice;
  }

  /**
   * Creates a GoSlice pointing to an array of GoSlice's. That is, creates an object which Go will
   * interpret as something of type <code>[][]byte</code>. It seems necessary to keep track of the
   * reference to the inner object outside this method to prevent weird issues where the memory of
   * the inner array will be reused (thus filling it with bogus data).
   *
   * @param bytes the data to fill in the value
   * @param innerPtr a reference to the inner array
   * @return a 2 dimensional GoSlice.
   */
  static PbctsTypes.GoSlice.ByValue bytesToGoSlice(
      List<byte[]> bytes, PbctsTypes.GoSlice.ByReference innerPtr) {
    int n = bytes.size();
    PbctsTypes.GoSlice[] slices = (PbctsTypes.GoSlice[]) innerPtr.toArray(n);
    for (int i = 0; i < n; i++) {
      byte[] innerBytes = bytes.get(i);
      int m = innerBytes.length;
      slices[i].len = m;
      slices[i].cap = m;
      if (m > 0) {
        // cannot allocate 0 bytes of memory.
        slices[i].data = new Memory(m);
        slices[i].data.write(0, innerBytes, 0, m);
      } else {
        slices[i].data = Memory.NULL;
      }
      // this tells JNA to actually write the content of this slice to memory. This is needed
      // because the type of the data field is void*, so write is not called when the outer struct
      // is created below.
      slices[i].write();
    }
    PbctsTypes.GoSlice.ByValue slice = new PbctsTypes.GoSlice.ByValue();
    slice.len = n;
    slice.cap = n;
    slice.data = innerPtr.getPointer();
    return slice;
  }

  /** A GoSlice. Stores arbitrary data. */
  @Structure.FieldOrder({"data", "len", "cap"})
  public abstract static class GoSlice extends Structure {

    public Pointer data;
    public long len;
    public long cap;

    /** Allow a GoSlice to be passed by values. */
    public static final class ByValue extends GoSlice implements Structure.ByValue {}

    /** Allow a GoSlice to be passed by reference. */
    public static final class ByReference extends GoSlice implements Structure.ByReference {}
  }

  /** A GoString. Stores a string of some fixed length. */
  @Structure.FieldOrder({"data", "size"})
  public abstract static class GoString extends Structure {

    public Pointer data;
    public long size;

    /** Allow a GoString to be passed by value. */
    public static final class ByValue extends GoString implements Structure.ByValue {}
  }

  // The next couple of types define custom extensions that makes the interface to the external
  // code more manageable.

  /**
   * Pointer to a known and fixed amount of data. Similar to {@link PbctsTypes.GoSlice}, however the
   * data pointed to by a Data object is not managed and so must be explicitely freed after use.
   */
  @Structure.FieldOrder({"size", "data"})
  public static final class Data extends Structure {

    public int size;
    public Pointer data;
  }

  /**
   * Like {@link PbctsTypes.Data} but includes an <code>error</code> field which signals whether or
   * not an error occurred while the value was being constructed. Specifically, the <code>data
   * </code> field can only be assumed to be meaningful if <code>error == 0</code>.
   */
  @Structure.FieldOrder({"error", "data"})
  public abstract static class DataAndError extends Structure {

    public int error;
    public Data data;

    /** Allow a DataAndError object to be passed by value. */
    public static final class ByValue extends DataAndError implements Structure.ByValue {}
  }

  /** A structure with an error and a integer. Same semantics as {@link PbctsTypes.DataAndError}. */
  @Structure.FieldOrder({"error", "val"})
  public abstract static class IntAndError extends Structure {

    public int error;
    public int val;

    /** Allow an IntAndError to be passed by value. */
    public static final class ByValue extends IntAndError implements Structure.ByValue {}
  }

  /**
   * A reference to something managed by Go. The reference is only meaningful if <code>
   * error == 0</code>.
   */
  @Structure.FieldOrder({"error", "reference"})
  public abstract static class Reference extends Structure {

    public int error;
    public long reference;

    /** Allow a Reference to be passed by value. */
    public static final class ByValue extends Reference implements Structure.ByValue {}
  }

  /** The result of running the first round of the key generation protocol. */
  @Structure.FieldOrder({"error", "unicastLengths", "unicastData", "broadcastData"})
  public abstract static class Round1Data extends Structure {

    public int error;
    public Data unicastLengths;
    public Data unicastData;
    public Data broadcastData;

    /** Allow a Round1Data object to be passed by value. */
    public static final class ByValue extends Round1Data implements Structure.ByValue {}
  }

  /** The result of running the final round of the protocol. */
  @Structure.FieldOrder({"error", "keyId", "preSigIds", "honestPlayers"})
  public abstract static class Round7Data extends Structure {

    public int error;
    public Pointer keyId;
    public Data preSigIds;
    public Data honestPlayers;

    /** Allow a Round7Data object to be passed by value. */
    public static final class ByValue extends Round7Data implements Structure.ByValue {}
  }

  /**
   * Container for a signature and a list of parties (given by their identifiers) which contributed
   * to the creation of the signature.
   */
  @Structure.FieldOrder({"error", "signature", "contributors"})
  public abstract static class CombinedSig extends Structure {

    public int error;
    public Data signature;
    public Data contributors;

    /** Allow a CombinedSig to be passed by value. */
    public static final class ByValue extends CombinedSig implements Structure.ByValue {}
  }
}
