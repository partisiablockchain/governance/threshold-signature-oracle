package com.partisiablockchain.governance.pbcts;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import java.math.BigInteger;
import java.util.Objects;

/** Helper class for creating signature objects. */
public final class SignatureHelper {

  private SignatureHelper() {}

  /**
   * Recover a signature that is valid for a given message and public key, from a serialized
   * signature.
   *
   * @param message the message that was signed
   * @param signatureBytes the serialized signature
   * @param publicKey the public key
   * @return a valid signature or null if no such signature was found
   */
  public static Signature signatureFromBytes(
      Hash message, byte[] signatureBytes, BlockchainPublicKey publicKey) {
    BigInteger r = new BigInteger(1, signatureBytes, 0, 32);
    BigInteger s = new BigInteger(1, signatureBytes, 32, 32);

    return findRecoveryId(message, r, s, publicKey);
  }

  static Signature findRecoveryId(
      Hash message, BigInteger r, BigInteger s, BlockchainPublicKey thisKey) {
    for (byte i = 0; i < 4; i++) {
      Signature signature = new Signature(i, r, s);
      BlockchainPublicKey blockchainPublicKey = signature.recoverPublicKey(message);
      if (Objects.equals(blockchainPublicKey, thisKey)) {
        return signature;
      }
    }
    // This should not happen
    return null;
  }
}
