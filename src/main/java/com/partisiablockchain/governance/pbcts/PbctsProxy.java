package com.partisiablockchain.governance.pbcts;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.governance.SignatureCombiningException;
import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.LongConsumer;

/** Proxy for interacting with pbcts-lib. */
public final class PbctsProxy {

  // for testing
  static final Consumer<Pointer> DEFAULT_POINTER_VACUUM = freePointer(Native::free);

  static Consumer<Pointer> freePointer(LongConsumer nativePointerVacuum) {
    return p -> nativePointerVacuum.accept(Pointer.nativeValue(p));
  }

  static final Pbcts DEFAULT_LIBRARY = Pbcts.INSTANCE;

  private final long playerReference;
  private final Pbcts externalLibrary;
  private final Consumer<Pointer> pointerVacuum;
  private Long sessionReference = null;

  PbctsProxy(long playerReference, Pbcts externalLibrary, Consumer<Pointer> pointerVacuum) {
    this.playerReference = playerReference;
    this.externalLibrary = externalLibrary;
    this.pointerVacuum = pointerVacuum;
  }

  private static PbctsProxy loadOrCreate(
      boolean create,
      String databaseName,
      String databaseBackupName,
      byte[] databaseKey,
      Pbcts externalLibrary,
      Consumer<Pointer> pointerVacuum) {
    PbctsTypes.GoString.ByValue dbName = PbctsTypes.stringToGoString(databaseName);
    PbctsTypes.GoString.ByValue dbBackupName = PbctsTypes.stringToGoString(databaseBackupName);
    PbctsTypes.GoSlice.ByValue dbKey = PbctsTypes.bytesToGoSlice(databaseKey);
    PbctsTypes.Reference.ByValue reference;

    if (create) {
      reference = externalLibrary.pbctsNew(dbName, dbBackupName, dbKey);
    } else {
      reference = externalLibrary.pbctsLoad(dbName, dbBackupName, dbKey);
    }

    if (reference.error == 1) {
      throw new RuntimeException("Could not obtain a valid player reference");
    }

    long playerReference = reference.reference;
    return new PbctsProxy(playerReference, externalLibrary, pointerVacuum);
  }

  static PbctsProxy load(
      String databaseName,
      String databaseBackupName,
      byte[] databaseKey,
      Pbcts externalLibrary,
      Consumer<Pointer> pointerVacuum) {
    return loadOrCreate(
        false, databaseName, databaseBackupName, databaseKey, externalLibrary, pointerVacuum);
  }

  /**
   * Create a pbcts proxy that uses an existing database.
   *
   * @param databaseName filename of the database
   * @param databaseBackupName filename of a backup database
   * @param databaseKey encryption key for the database
   * @return a proxy.
   */
  public static PbctsProxy load(
      String databaseName, String databaseBackupName, byte[] databaseKey) {
    return load(
        databaseName, databaseBackupName, databaseKey, DEFAULT_LIBRARY, DEFAULT_POINTER_VACUUM);
  }

  static PbctsProxy create(
      String databaseName,
      String databaseBackupName,
      byte[] databaseKey,
      Pbcts externalLibrary,
      Consumer<Pointer> pointerVacuum) {
    return loadOrCreate(
        true, databaseName, databaseBackupName, databaseKey, externalLibrary, pointerVacuum);
  }

  /**
   * Create a pbcts proxy with a new database. The filename name must not point to an existing file.
   *
   * @param databaseName filename of the database
   * @param databaseBackupName filename of a backup database
   * @param databaseKey encryption key of the database
   * @return a proxy.
   */
  public static PbctsProxy create(
      String databaseName, String databaseBackupName, byte[] databaseKey) {
    return create(
        databaseName, databaseBackupName, databaseKey, DEFAULT_LIBRARY, DEFAULT_POINTER_VACUUM);
  }

  /**
   * Get the public key associated with a key ID.
   *
   * @param keyId the key ID
   * @return a public key.
   * @throws RuntimeException if it was not possible to obtain the public key.
   */
  public BlockchainPublicKey getPublicKey(String keyId) {
    PbctsTypes.GoString.ByValue goString = PbctsTypes.stringToGoString(keyId);
    PbctsTypes.DataAndError.ByValue data =
        externalLibrary.pbctsPublicKey(playerReference, goString);
    if (data.error == 1) {
      throw new RuntimeException("Could not obtain public key");
    }
    byte[] bytes = dataToBytes(data.data);
    return BlockchainPublicKey.fromEncodedEcPoint(bytes);
  }

  /**
   * Partially sign a message.
   *
   * @param keyId the ID of the key share to use for signing
   * @param preSignatureId the ID of the pre-signature to use
   * @param message the message to sign
   * @param preSignatureRandomizer randomization data. Cannot be empty
   * @return a partial signature.
   * @throws RuntimeException if partial signing failed.
   */
  public byte[] sign(
      String keyId, String preSignatureId, Hash message, byte[] preSignatureRandomizer) {
    PbctsTypes.GoString.ByValue key = PbctsTypes.stringToGoString(keyId);
    PbctsTypes.GoString.ByValue preSigId = PbctsTypes.stringToGoString(preSignatureId);
    PbctsTypes.GoSlice.ByValue messageBytes = PbctsTypes.bytesToGoSlice(message.getBytes());
    PbctsTypes.GoSlice.ByValue preSigRand = PbctsTypes.bytesToGoSlice(preSignatureRandomizer);

    PbctsTypes.DataAndError.ByValue data =
        externalLibrary.pbctsSign(playerReference, key, preSigId, messageBytes, preSigRand);

    if (data.error == 1) {
      throw new RuntimeException("Could not sign message");
    }

    return dataToBytes(data.data);
  }

  /**
   * Combine a list of partial signatures into a signature.
   *
   * @param partialSignatures the partial signatures
   * @param message the message that was signed
   * @param publicKey the public key that the signature should be valid for
   * @return a signature.
   * @throws RuntimeException if combination of the partial signatures failed.
   */
  public CombinedSignature combine(
      List<byte[]> partialSignatures, Hash message, BlockchainPublicKey publicKey) {
    PbctsTypes.GoSlice.ByReference innerPtr = new PbctsTypes.GoSlice.ByReference();
    PbctsTypes.GoSlice.ByValue partialSigs = PbctsTypes.bytesToGoSlice(partialSignatures, innerPtr);

    PbctsTypes.CombinedSig.ByValue data = externalLibrary.pbctsCombine(partialSigs);

    if (data.error == 1) {
      throw new SignatureCombiningException("Could not combine partial signatures");
    }

    List<Long> contributors = dataToLongs(data.contributors);
    byte[] signatureBytes = dataToBytes(data.signature);
    return new CombinedSignature(
        SignatureHelper.signatureFromBytes(message, signatureBytes, publicKey), contributors);
  }

  /**
   * Delete a secret key share.
   *
   * @param keyId the ID associated with the key share
   * @return true if deletion succeeded and false otherwise.
   */
  public boolean deleteKeyShare(String keyId) {
    PbctsTypes.GoString.ByValue key = PbctsTypes.stringToGoString(keyId);
    return externalLibrary.pbctsDeleteKeyShare(playerReference, key);
  }

  /**
   * Delete a pre-signature.
   *
   * @param keyId the key ID for the pre-signature
   * @param preSignatureId the ID of the pre-signature
   * @return true if deletion succeeded and false otherwise.
   */
  public boolean deletePreSignature(String keyId, String preSignatureId) {
    PbctsTypes.GoString.ByValue key = PbctsTypes.stringToGoString(keyId);
    PbctsTypes.GoString.ByValue preSigId = PbctsTypes.stringToGoString(preSignatureId);
    return externalLibrary.pbctsDeletePreSignature(playerReference, key, preSigId);
  }

  /**
   * Delete all pre-signatures for a given key ID.
   *
   * @param keyId the key ID
   * @return true if deletion succeeded and false otherwise.
   */
  public boolean deleteAllPreSignatures(String keyId) {
    PbctsTypes.GoString.ByValue key = PbctsTypes.stringToGoString(keyId);
    return externalLibrary.pbctsDeleteAllPreSignatures(playerReference, key);
  }

  /**
   * Get the total number of pre-signatures for a provided key ID.
   *
   * @param keyId the key ID
   * @return the number of pre-signatures.
   */
  public int preSignatureCount(String keyId) {
    PbctsTypes.GoString.ByValue key = PbctsTypes.stringToGoString(keyId);
    PbctsTypes.IntAndError.ByValue count =
        externalLibrary.pbctsPreSignatureCount(playerReference, key);

    if (count.error == 1) {
      throw new RuntimeException("Could not count pre-signatures");
    }

    return count.val;
  }

  /**
   * Get all pre-signatures for a given key ID.
   *
   * @param keyId the key ID
   * @return a list of pre-signatures.
   */
  public List<String> getPreSignatureIds(String keyId) {
    PbctsTypes.GoString.ByValue key = PbctsTypes.stringToGoString(keyId);
    PbctsTypes.DataAndError.ByValue data =
        externalLibrary.pbctsListPreSignatureIds(playerReference, key);

    if (data.error == 1) {
      throw new RuntimeException("Could not list pre-signatures");
    }

    return dataToStringList(data.data);
  }

  /**
   * Initialize the key generation protocol.
   *
   * @param id the ID of this party in the protocol
   * @param n the total number of parties in the protocol
   * @param preSigCount the number of pre-signatures to generate
   * @param sessionId the session ID to use for this key-gen session
   */
  public void initializeKeyGen(long id, long n, long preSigCount, String sessionId) {
    PbctsTypes.GoString.ByValue sid = PbctsTypes.stringToGoString(sessionId);
    PbctsTypes.Reference.ByValue session =
        externalLibrary.pbctsNewKeyGenSession(playerReference, id, n, preSigCount, sid);
    if (session.error == 1) {
      throw new RuntimeException("Could not initialize a new key-gen session");
    }
    this.sessionReference = session.reference;
  }

  /**
   * Resume a key-gen session given a session ID.
   *
   * @param sessionId the session ID
   */
  public void resumeKeyGen(String sessionId) {
    PbctsTypes.GoString.ByValue sid = PbctsTypes.stringToGoString(sessionId);
    PbctsTypes.Reference.ByValue session =
        externalLibrary.pbctsResumeKeyGenSession(playerReference, sid);
    if (session.error == 1) {
      throw new RuntimeException("Could not resume key-gen session for session: " + sessionId);
    }
    this.sessionReference = session.reference;
  }

  /** Close this protocol. */
  public void shutdown() {
    boolean success = externalLibrary.pbctsFlushKeyGenSessionCache(playerReference);
    if (!success) {
      throw new RuntimeException("Error encountered while flushing key-gen session cache");
    }
  }

  /**
   * Runs round 1 of the key generation protocol.
   *
   * @return data for round 2.
   */
  public Round1Data round1() {
    PbctsTypes.Round1Data.ByValue round1Data = externalLibrary.pbctsRound1(sessionReference);
    if (round1Data.error == 1) {
      throw new RuntimeException("Error executing round 1");
    }
    byte[] broadcastData = dataToBytes(round1Data.broadcastData);
    List<byte[]> unicastData = dataToListBytes(round1Data.unicastData, round1Data.unicastLengths);
    return new Round1Data(unicastData, broadcastData);
  }

  /**
   * Runs round 2 of the key generation protocol.
   *
   * @param unicastMessages unicast messages from round 1
   * @param broadcastMessages broadcast messages from round 1
   * @return data for round 3.
   */
  public byte[] round2(List<byte[]> unicastMessages, List<byte[]> broadcastMessages) {
    PbctsTypes.GoSlice.ByReference innerPtrUc = new PbctsTypes.GoSlice.ByReference();
    PbctsTypes.GoSlice.ByValue unicast = PbctsTypes.bytesToGoSlice(unicastMessages, innerPtrUc);
    PbctsTypes.GoSlice.ByReference innerPtrBc = new PbctsTypes.GoSlice.ByReference();
    PbctsTypes.GoSlice.ByValue broadcast = PbctsTypes.bytesToGoSlice(broadcastMessages, innerPtrBc);

    PbctsTypes.DataAndError.ByValue round2Data =
        externalLibrary.pbctsRound2(sessionReference, unicast, broadcast);

    if (round2Data.error == 1) {
      throw new RuntimeException("Error executing round 2");
    }

    return dataToBytes(round2Data.data);
  }

  /**
   * Runs round 3 of the key generation protocol.
   *
   * @param broadcastMessages broadcast messages from round 2
   * @return data for round 4.
   */
  public byte[] round3(List<byte[]> broadcastMessages) {
    return executeIntermediaryRound(broadcastMessages, 3);
  }

  /**
   * Runs round 4 of the key generation protocol.
   *
   * @param broadcastMessages broadcast messages from round 3
   * @return data for round 5.
   */
  public byte[] round4(List<byte[]> broadcastMessages) {
    return executeIntermediaryRound(broadcastMessages, 4);
  }

  /**
   * Runs round 5 of the key generation protocol.
   *
   * @param broadcastMessages broadcast messages from round 4
   * @return data for round 6.
   */
  public byte[] round5(List<byte[]> broadcastMessages) {
    return executeIntermediaryRound(broadcastMessages, 5);
  }

  /**
   * Runs round 6 of the key generation protocol.
   *
   * @param broadcastMessages broadcast messages from round 5
   * @return data for round 7.
   */
  public byte[] round6(List<byte[]> broadcastMessages) {
    return executeIntermediaryRound(broadcastMessages, 6);
  }

  /**
   * Runs round 7 (the final) round of the key generation protocol.
   *
   * @param broadcastMessages broadcast messages from round 6
   * @return a key ID, list of pre-signatures and list of honest parties.
   */
  public KeyGenerationResult round7(List<byte[]> broadcastMessages) {
    PbctsTypes.GoSlice.ByReference innerPtr = new PbctsTypes.GoSlice.ByReference();
    PbctsTypes.GoSlice.ByValue data = PbctsTypes.bytesToGoSlice(broadcastMessages, innerPtr);
    PbctsTypes.Round7Data.ByValue result = externalLibrary.pbctsRound7(sessionReference, data);

    if (result.error == 1) {
      throw new RuntimeException("Error executing round 7");
    }

    String keyId = pointerToString(result.keyId);
    List<String> preSigIds = dataToStringList(result.preSigIds);
    List<Long> honestPlayers = dataToLongs(result.honestPlayers);
    return new KeyGenerationResult(keyId, preSigIds, honestPlayers);
  }

  /**
   * Close the database.
   *
   * @return true if closing was successful and false otherwise.
   */
  public boolean close() {
    return externalLibrary.pbctsClose(playerReference);
  }

  /**
   * Delete the key generation session.
   *
   * @return true if deletion succeeded and false otherwise.
   */
  public boolean deleteSession() {
    return externalLibrary.pbctsDeleteSession(sessionReference);
  }

  private byte[] executeIntermediaryRound(List<byte[]> messagesFromPreviousRound, int roundNumber) {
    PbctsTypes.GoSlice.ByReference innerPtr = new PbctsTypes.GoSlice.ByReference();
    PbctsTypes.GoSlice.ByValue data =
        PbctsTypes.bytesToGoSlice(messagesFromPreviousRound, innerPtr);
    PbctsTypes.DataAndError.ByValue result;
    if (roundNumber == 3) {
      result = externalLibrary.pbctsRound3(sessionReference, data);
    } else if (roundNumber == 4) {
      result = externalLibrary.pbctsRound4(sessionReference, data);
    } else if (roundNumber == 5) {
      result = externalLibrary.pbctsRound5(sessionReference, data);
    } else { // if (roundNumber == 6)
      result = externalLibrary.pbctsRound6(sessionReference, data);
    }

    if (result.error == 1) {
      throw new RuntimeException("Error executing round " + roundNumber);
    }

    return dataToBytes(result.data);
  }

  private String pointerToString(Pointer ptr) {
    String string = ptr.getString(0);
    free(ptr);
    return string;
  }

  private byte[] dataToBytes(PbctsTypes.Data data) {
    byte[] byteArray = data.data.getByteArray(0, data.size);
    free(data.data);
    return byteArray;
  }

  private List<byte[]> dataToListBytes(PbctsTypes.Data data, PbctsTypes.Data sizes) {
    List<Long> arraySizes = dataToLongs(sizes);
    List<byte[]> bytes = new ArrayList<>();
    int offset = 0;
    for (long size : arraySizes) {
      bytes.add(data.data.getByteArray(offset, (int) size));
      offset = (int) (offset + size);
    }
    free(data.data);
    return bytes;
  }

  private List<Long> dataToLongs(PbctsTypes.Data data) {
    long[] longs = data.data.getLongArray(0, data.size);
    free(data.data);
    return Arrays.stream(longs).boxed().toList();
  }

  private List<String> dataToStringList(PbctsTypes.Data data) {
    List<String> strings = new ArrayList<>();
    int c = 0;
    for (int i = 0; i < data.size; i++) {
      String s = data.data.getString(c);
      c += s.length() + 1;
      strings.add(s);
    }
    free(data.data);
    // this is to ensure that the returned list is always returned in a consistent order.
    return strings.stream().sorted().toList();
  }

  private void free(Pointer data) {
    this.pointerVacuum.accept(data);
  }

  /** Result of the round 1 of the key generation protocol. */
  public static final class Round1Data {

    /** Unicast messages. */
    public List<byte[]> unicastMessages;

    /** Broadcast messages. */
    public byte[] broadcastMessage;

    Round1Data(List<byte[]> unicastMessages, byte[] broadcastMessage) {
      this.unicastMessages = unicastMessages;
      this.broadcastMessage = broadcastMessage;
    }
  }

  /** Result of the final round of the key generation protocol. */
  public static final class KeyGenerationResult {

    /** ID of the newly generated key. */
    public String keyId;

    /** Pre-signatures. */
    public List<String> preSignatureIds;

    /** IDs of players who acted honestly in the key generation protocol. */
    public List<Long> honestPlayers;

    KeyGenerationResult(String keyId, List<String> preSignatureIds, List<Long> honestPlayers) {
      this.keyId = keyId;
      this.preSignatureIds = preSignatureIds;
      this.honestPlayers = honestPlayers;
    }
  }

  /**
   * The result of combining a signature. Is in effect a pair (s, c) where s is the signature bytes
   * and c is a list of identifiers of the parties which contributed to the reconstruction.
   */
  public static final class CombinedSignature {

    /** Signature. */
    public final Signature signature;

    /** Contributors. */
    public final List<Long> contributors;

    /**
     * Default constructor.
     *
     * @param signature the signature
     * @param contributors the contributors
     */
    public CombinedSignature(Signature signature, List<Long> contributors) {
      this.signature = signature;
      this.contributors = contributors;
    }
  }

  /** Bindings for external functions in Go. */
  interface Pbcts extends Library {

    Pbcts INSTANCE = (Pbcts) Native.synchronizedLibrary(Native.load("pbcts.so", Pbcts.class));

    /**
     * Create a new player reference. A player reference is effectively a pointer to a database
     * object, and optionally a backup database.
     *
     * @param databaseFilename the name of the database
     * @param databaseBackupFilename the name of the backup database
     * @param databaseKey encryption key for the database
     * @return a fresh player reference.
     */
    PbctsTypes.Reference.ByValue pbctsNew(
        PbctsTypes.GoString.ByValue databaseFilename,
        PbctsTypes.GoString.ByValue databaseBackupFilename,
        PbctsTypes.GoSlice.ByValue databaseKey);

    /**
     * Obtain a player reference to an existing database.
     *
     * @param databaseFilename the name of the database
     * @param databaseBackupFilename the name of the backup database
     * @param databaseKey encryption key for the database
     * @return a fresh player reference.
     */
    PbctsTypes.Reference.ByValue pbctsLoad(
        PbctsTypes.GoString.ByValue databaseFilename,
        PbctsTypes.GoString.ByValue databaseBackupFilename,
        PbctsTypes.GoSlice.ByValue databaseKey);

    /**
     * Create a new key generation protocol object and return a reference to it.
     *
     * @param playerReference a player reference
     * @param playerIndex the index of this player in the key-gen protocol
     * @param playerCount the total number of players in the key-gen protocol
     * @param preSignatureCount the number of pre-signatures to generate in this key-gen protocol
     * @param sessionId a unique session ID to identify this session from
     * @return a fresh session reference.
     */
    PbctsTypes.Reference.ByValue pbctsNewKeyGenSession(
        long playerReference,
        long playerIndex,
        long playerCount,
        long preSignatureCount,
        PbctsTypes.GoString.ByValue sessionId);

    /**
     * Attempt to resume a key-gen session given a session ID and database (player) reference.
     *
     * @param playerReference a player reference
     * @param sessionId a session ID
     * @return a key-gen session reference.
     */
    PbctsTypes.Reference.ByValue pbctsResumeKeyGenSession(
        long playerReference, PbctsTypes.GoString.ByValue sessionId);

    /**
     * Flush and invalidate data held in memory for all sessions by this player.
     *
     * @param playerReference a player reference
     * @return true if everything went well.
     */
    boolean pbctsFlushKeyGenSessionCache(long playerReference);

    /**
     * Get a threshold public key.
     *
     * @param playerReference a player reference
     * @param keyId the associated key ID, as output when the key-gen protocol has finished
     * @return a threshold public key.
     */
    PbctsTypes.DataAndError.ByValue pbctsPublicKey(
        long playerReference, PbctsTypes.GoString.ByValue keyId);

    /**
     * Perform a partial signing of a message.
     *
     * @param playerReference a player reference
     * @param keyId the ID of the key to use
     * @param preSignatureId the ID of the pre-signature to use
     * @param message the message to partial sign
     * @param preSignatureRandomizer a piece of random information to re-randomize the pre-signature
     *     before signing.
     * @return a partial signature
     */
    PbctsTypes.DataAndError.ByValue pbctsSign(
        long playerReference,
        PbctsTypes.GoString.ByValue keyId,
        PbctsTypes.GoString.ByValue preSignatureId,
        PbctsTypes.GoSlice.ByValue message,
        PbctsTypes.GoSlice.ByValue preSignatureRandomizer);

    /**
     * Combine a list of partial signatures (as returned by {@link Pbcts#pbctsSign} into a proper
     * ECDSA signature.
     *
     * @param partialSignatures the list of partial signatures
     * @return a signature and a list of parties who contributed to the signature.
     */
    PbctsTypes.CombinedSig.ByValue pbctsCombine(PbctsTypes.GoSlice.ByValue partialSignatures);

    /**
     * Delete the secret key share associated with a key ID.
     *
     * @param playerReference a player reference
     * @param keyId the ID of the key share to delete
     * @return true if the key share was deleted and false otherwise.
     */
    boolean pbctsDeleteKeyShare(long playerReference, PbctsTypes.GoString.ByValue keyId);

    /**
     * Delete a particular pre-signature.
     *
     * @param playerReference a player reference
     * @param keyId the ID of the key the pre-signature is associated with
     * @param preSignatureId the ID of the pre-signature to delete
     * @return true if the pre-signature was deleted and false otherwise.
     */
    boolean pbctsDeletePreSignature(
        long playerReference,
        PbctsTypes.GoString.ByValue keyId,
        PbctsTypes.GoString.ByValue preSignatureId);

    /**
     * Delete all pre-signatures associated with a particular key.
     *
     * @param playerReference a player reference
     * @param keyId the ID of the key whose pre-signatures to delete
     * @return true if the pre-signatures were deleted successfully and false otherwise.
     */
    boolean pbctsDeleteAllPreSignatures(long playerReference, PbctsTypes.GoString.ByValue keyId);

    /**
     * Get the number of available pre-signatures for a particular key.
     *
     * @param playerReference a player reference
     * @param keyId the ID of the key whose pre-signatures to count
     * @return the number of pre-signatures.
     */
    PbctsTypes.IntAndError.ByValue pbctsPreSignatureCount(
        long playerReference, PbctsTypes.GoString.ByValue keyId);

    /**
     * Get the pre-signature IDs for a particular key.
     *
     * @param playerReference a player reference
     * @param keyId the ID of the key whose pre-signatures to return
     * @return a list of pre-signatures.
     */
    PbctsTypes.DataAndError.ByValue pbctsListPreSignatureIds(
        long playerReference, PbctsTypes.GoString.ByValue keyId);

    /**
     * Round 1 of the key-gen protocol.
     *
     * @param sessionReference a session reference
     * @return data for round 2 -- a collection of unicast messages and a broadcast message.
     */
    PbctsTypes.Round1Data.ByValue pbctsRound1(long sessionReference);

    /**
     * Round 2 of the key-gen protocol.
     *
     * @param sessionReference a session reference
     * @param round1UnicastMessages unicast messages from round 1
     * @param round1BroadcastMessages broadcast messages from round 1
     * @return broadcast messages for round 3
     */
    PbctsTypes.DataAndError.ByValue pbctsRound2(
        long sessionReference,
        PbctsTypes.GoSlice.ByValue round1UnicastMessages,
        PbctsTypes.GoSlice.ByValue round1BroadcastMessages);

    /**
     * Round 3 of the key-gen protocol.
     *
     * @param sessionReference a session reference
     * @param round2BroadcastMessages broadcast messages from round 2
     * @return broadcast messages for round 4.
     */
    PbctsTypes.DataAndError.ByValue pbctsRound3(
        long sessionReference, PbctsTypes.GoSlice.ByValue round2BroadcastMessages);

    /**
     * Round 4 of the key-gen protocol.
     *
     * @param sessionReference a session reference
     * @param round3BroadcastMessages broadcast messages from round 3
     * @return broadcast messages for round 5.
     */
    PbctsTypes.DataAndError.ByValue pbctsRound4(
        long sessionReference, PbctsTypes.GoSlice.ByValue round3BroadcastMessages);

    /**
     * Round 5 of the key-gen protocol.
     *
     * @param sessionReference a session reference
     * @param round4BroadcastMessages broadcast messages from round 4
     * @return broadcast messages for round 6.
     */
    PbctsTypes.DataAndError.ByValue pbctsRound5(
        long sessionReference, PbctsTypes.GoSlice.ByValue round4BroadcastMessages);

    /**
     * Round 6 of the key-gen protocol.
     *
     * @param sessionReference a session reference
     * @param round5BroadcastMessages broadcast messages from round 5
     * @return broadcast messages for round 7.
     */
    PbctsTypes.DataAndError.ByValue pbctsRound6(
        long sessionReference, PbctsTypes.GoSlice.ByValue round5BroadcastMessages);

    /**
     * Round 7 of the key-gen protocol. This is the final round of the key-gen protocol.
     *
     * @param sessionReference a session reference
     * @param round6BroadcastMessages broadcast messages from round 6
     * @return a key ID, list of pre-signature IDs and a list of honest parties.
     */
    PbctsTypes.Round7Data.ByValue pbctsRound7(
        long sessionReference, PbctsTypes.GoSlice.ByValue round6BroadcastMessages);

    /**
     * Close the databases referenced by a player reference.
     *
     * @param playerReference the player reference
     * @return true if the player's databases where closed successfully and false otherwise.
     */
    boolean pbctsClose(long playerReference);

    /**
     * Delete a key generation session object referenced by a session reference.
     *
     * @param sessionReference the session reference
     * @return true if the session was deleted successfully and false otherwise.
     */
    boolean pbctsDeleteSession(long sessionReference);
  }
}
