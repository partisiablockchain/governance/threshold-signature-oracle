package com.partisiablockchain.governance;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.BlockchainPublicKey;

/** Defines retrieval of messages for a threshold signature protocol or signature provider. */
public interface KeyGenMessageRetriever {

  /**
   * Get a message from some party.
   *
   * @param sender the sender of the message
   * @return the message.
   */
  byte[] retrieveUnicastMessage(BlockchainPublicKey sender);

  /**
   * Get a broadcast message from some party.
   *
   * @param sender the sender
   * @return the message.
   */
  byte[] retrieveBroadcastMessage(BlockchainPublicKey sender);
}
