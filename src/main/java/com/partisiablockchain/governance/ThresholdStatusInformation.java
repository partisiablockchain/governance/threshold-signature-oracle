package com.partisiablockchain.governance;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/** Accessing information for a given key generation session. */
public interface ThresholdStatusInformation {

  /**
   * Returns the current round number for the threshold signing session.
   *
   * @param thresholdSessionId session id.
   * @return current round number
   */
  int getCurrentRoundNumber(ThresholdSessionId thresholdSessionId);

  /**
   * Returns the number of unused signature ids in the threshold signing session.
   *
   * @param thresholdSessionId session id.
   * @return number of unused signature ids.
   */
  int getUnusedSignatureIds(ThresholdSessionId thresholdSessionId);

  /**
   * Returns the number of parties that acted honest doing the protocol.
   *
   * @param thresholdSessionId session id.
   * @return number of honest parties.
   */
  int getHonestParties(ThresholdSessionId thresholdSessionId);
}
