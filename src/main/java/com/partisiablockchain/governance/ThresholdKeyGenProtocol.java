package com.partisiablockchain.governance;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/** A threshold key generation and pre-signature generation protocol. */
public abstract class ThresholdKeyGenProtocol {

  /** A step of the key generation protocol. */
  public abstract class Step {

    /**
     * Get the current round number.
     *
     * @return the current round number.
     */
    public int roundNumber() {
      return getRoundNumber();
    }
  }

  /** An internal step of the protocol. */
  public abstract class InternalStep extends Step {

    /**
     * Advance a step in the protocol.
     *
     * @param retriever a retriever for receiving messages
     * @return the next step of the protocol.
     */
    public abstract Step advance(KeyGenMessageRetriever retriever);
  }

  /** Final step of the key generation protocol. */
  public abstract class FinalStep extends Step {

    /**
     * Finish the protocol and return an object that can threshold sign messages.
     *
     * @return a threshold signer.
     */
    public abstract ThresholdSignatureProvider finish();
  }

  /**
   * Initialize the threshold key generation protocol.
   *
   * @return the first step of the protocol.
   */
  public abstract Step init();

  /**
   * Resume the key-gen session.
   *
   * @param currentRoundNumber the current round number
   * @return the current step of the protocol.
   */
  public abstract Step resume(int currentRoundNumber);

  /**
   * Get the current round number of the protocol.
   *
   * @return the current round number of the protocol.
   */
  public abstract int getRoundNumber();

  /** Shutdown this key-gen session. */
  public abstract void shutdown();
}
