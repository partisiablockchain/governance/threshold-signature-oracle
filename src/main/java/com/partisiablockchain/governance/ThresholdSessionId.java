package com.partisiablockchain.governance;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.Objects;

/**
 * Identifier for threshold protocol sessions. A session is a pair (sid, rn) where the former is a
 * session ID, which identifies the oracle, and the latter is a nonce indicating the number of tries
 * it took to generate a threshold key.
 */
public final class ThresholdSessionId {

  private final int sessionId;
  private final int retryNonce;

  /**
   * Create a new threshold session ID.
   *
   * @param sessionId the oracle session id
   * @param retryNonce the retry nonce
   */
  public ThresholdSessionId(int sessionId, int retryNonce) {
    this.sessionId = sessionId;
    this.retryNonce = retryNonce;
  }

  /**
   * Gets the oracle session id.
   *
   * @return the session id.
   */
  public int getSessionId() {
    return sessionId;
  }

  /**
   * Gets the retry nonce.
   *
   * @return the retry nonce.
   */
  public int getRetryNonce() {
    return retryNonce;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ThresholdSessionId that = (ThresholdSessionId) o;
    return sessionId == that.sessionId && retryNonce == that.retryNonce;
  }

  @Override
  public int hashCode() {
    return Objects.hash(sessionId, retryNonce);
  }

  @Override
  public String toString() {
    return "(sessionId=" + sessionId + ", retryNonce=" + retryNonce + ')';
  }
}
