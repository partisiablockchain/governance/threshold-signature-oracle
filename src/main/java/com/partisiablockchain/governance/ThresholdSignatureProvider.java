package com.partisiablockchain.governance;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import java.util.List;

/**
 * Interface of a threshold signature protocol that utilizes pre-signatures and partial signatures.
 */
public interface ThresholdSignatureProvider {

  /**
   * Get the threshold public key. Is allowed to return null in case no key has been generated yet.
   *
   * @return the public key.
   */
  ThresholdPublicKey getPublicKey();

  /**
   * Get the number of signatures left that this protocol can create.
   *
   * @return the number of pre-signatures that are left.
   */
  int getRemainingSignatures();

  /**
   * Get pre-signature IDs.
   *
   * @return pre-signature IDs.
   */
  List<String> getSignatureIds();

  /**
   * Partially sign a message using a signature index.
   *
   * @param message the message to sign
   * @param signatureId a signature ID
   * @param preSignatureRandomizer some random bytes for randomization. Cannot be empty
   */
  void preSignAndStore(Hash message, String signatureId, byte[] preSignatureRandomizer);

  /**
   * Attempt combination of existing pre-signatures.
   *
   * @param retriever retriever for receiving pre-signatures
   * @return a list of all signatures that it was possible to complete.
   */
  List<SignatureAndContributors> collectNewSignatures(PartialSignatureRetriever retriever);

  /**
   * Get the list of parties that were determined to have acted honestly during the key generation
   * protocol.
   *
   * @return the list of honest parties.
   */
  List<BlockchainPublicKey> getHonestParties();
}
