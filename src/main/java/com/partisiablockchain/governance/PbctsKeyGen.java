package com.partisiablockchain.governance;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.governance.pbcts.PbctsProxy;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;

/** Threshold ECDSA key generation protocol based on pbcts-lib. */
public final class PbctsKeyGen extends ThresholdKeyGenProtocol {

  private final ThresholdSessionId sessionId;
  private final List<BlockchainPublicKey> parties;
  private final BlockchainPublicKey me;
  private final MessageStorage storage;
  private final int preSignatureCount;
  private final PbctsProxy proxy;
  private final Consumer<PbctsProxy> shutdownHook;

  private int currentRoundNumber = 1;
  static final int MAX_ROUNDS = 7;

  private PbctsProxy.KeyGenerationResult keyGenerationResult = null;

  static final Consumer<PbctsProxy> DEFAULT_SHUTDOWN_HOOK =
      proxy -> {
        proxy.shutdown();
        proxy.close();
      };

  /**
   * Create a new key generation protocol with a fresh database.
   *
   * @param sessionId the session id of this protocol
   * @param parties the participants
   * @param me this party
   * @param storage a storage object
   * @param preSignatureCount the number of pre-signatures to generate
   * @param databaseName the name of the database. Cannot point to an existing file
   * @param databaseBackupName the name of a backup database
   * @param databaseKey encryption key for the database
   * @return a key generation protocol object.
   */
  public static PbctsKeyGen create(
      ThresholdSessionId sessionId,
      List<BlockchainPublicKey> parties,
      BlockchainPublicKey me,
      MessageStorage storage,
      int preSignatureCount,
      String databaseName,
      String databaseBackupName,
      byte[] databaseKey) {
    return new PbctsKeyGen(
        sessionId,
        parties,
        me,
        storage,
        preSignatureCount,
        databaseName,
        databaseBackupName,
        databaseKey,
        false,
        DEFAULT_SHUTDOWN_HOOK);
  }

  /**
   * Create a new key generation protocol object that points to an existing database.
   *
   * @param sessionId the session id of this protocol
   * @param parties the participants
   * @param me this party
   * @param storage a storage object
   * @param preSignatureCount the number of pre-signatures to generate
   * @param databaseName name of the database. Must point to an existing database
   * @param databaseBackupName name of a backup database
   * @param databaseKey database encryption key
   * @return a key generation protocol object.
   */
  public static PbctsKeyGen load(
      ThresholdSessionId sessionId,
      List<BlockchainPublicKey> parties,
      BlockchainPublicKey me,
      MessageStorage storage,
      int preSignatureCount,
      String databaseName,
      String databaseBackupName,
      byte[] databaseKey) {
    return new PbctsKeyGen(
        sessionId,
        parties,
        me,
        storage,
        preSignatureCount,
        databaseName,
        databaseBackupName,
        databaseKey,
        true,
        DEFAULT_SHUTDOWN_HOOK);
  }

  PbctsKeyGen(
      ThresholdSessionId sessionId,
      List<BlockchainPublicKey> parties,
      BlockchainPublicKey me,
      MessageStorage storage,
      int preSignatureCount,
      String databaseName,
      String databaseBackupName,
      byte[] databaseKey,
      boolean loadInsteadOfCreate,
      Consumer<PbctsProxy> shutdownHook) {
    this.sessionId = sessionId;
    this.parties = parties;
    this.me = me;
    this.storage = storage;
    this.preSignatureCount = preSignatureCount;
    this.shutdownHook = shutdownHook;
    if (loadInsteadOfCreate) {
      this.proxy = PbctsProxy.load(databaseName, databaseBackupName, databaseKey);
    } else {
      this.proxy = PbctsProxy.create(databaseName, databaseBackupName, databaseKey);
    }
  }

  @Override
  public Step init() {
    int myId = parties.indexOf(me);
    proxy.initializeKeyGen(myId, parties.size(), preSignatureCount, sessionIdToString());
    PbctsProxy.Round1Data round1Data = proxy.round1();
    currentRoundNumber = 2;
    storeRound1Messages(round1Data);
    return theSecondRound();
  }

  @Override
  public Step resume(int currentRoundNumber) {
    proxy.resumeKeyGen(sessionIdToString());
    this.currentRoundNumber = currentRoundNumber;
    if (currentRoundNumber == 2) {
      return theSecondRound();
    } else {
      return anInternalRound();
    }
  }

  @Override
  public void shutdown() {
    shutdownHook.accept(proxy);
  }

  /**
   * Convert a ThresholdSessionId to a string.
   *
   * @return a fixed length string representation of a session ID.
   */
  String sessionIdToString() {
    return Hash.create(
            s -> {
              s.writeString("LargeOracleKeyGenSession");
              s.writeInt(sessionId.getSessionId());
              s.writeInt(sessionId.getRetryNonce());
            })
        .toString();
  }

  @Override
  public int getRoundNumber() {
    return currentRoundNumber;
  }

  private Step round2(KeyGenMessageRetriever retriever) {
    List<byte[]> unicastData = new ArrayList<>();
    List<byte[]> broadcastData = new ArrayList<>();
    for (BlockchainPublicKey party : parties) {
      byte[] bcData = retriever.retrieveBroadcastMessage(party);
      byte[] ucData = retriever.retrieveUnicastMessage(party);
      broadcastData.add(Objects.requireNonNullElseGet(bcData, () -> new byte[0]));
      unicastData.add(Objects.requireNonNullElseGet(ucData, () -> new byte[0]));
    }

    advanceRound(proxy.round2(unicastData, broadcastData));
    return anInternalRound();
  }

  private Step runInternalStep(KeyGenMessageRetriever retriever) {
    advanceRound(executeRound(getBroadcastMessages(retriever)));
    if (currentRoundNumber == MAX_ROUNDS) {
      return theLastRound();
    } else {
      return anInternalRound();
    }
  }

  private Step round7(KeyGenMessageRetriever retriever) {
    keyGenerationResult = proxy.round7(getBroadcastMessages(retriever));
    return createSignatureProvider();
  }

  private void storeRound1Messages(PbctsProxy.Round1Data round1Data) {
    byte[] broadcastData = round1Data.broadcastMessage;
    storage.setBroadcastMessage(sessionId, currentRoundNumber, broadcastData);
    for (int i = 0; i < parties.size(); i++) {
      BlockchainPublicKey party = parties.get(i);
      byte[] unicastData = round1Data.unicastMessages.get(i);
      storage.setRoundUnicastMessage(sessionId, currentRoundNumber, party, unicastData);
    }
  }

  private void advanceRound(byte[] broadcastData) {
    currentRoundNumber++;
    storage.setBroadcastMessage(sessionId, currentRoundNumber, broadcastData);
  }

  private FinalStep createSignatureProvider() {
    return new FinalStep() {
      @Override
      public ThresholdSignatureProvider finish() {
        return new PbctsSigner(
            keyGenerationResult.keyId,
            sessionId,
            keyGenerationResult.preSignatureIds,
            parties,
            me,
            storage,
            keyGenerationResult.honestPlayers,
            proxy);
      }
    };
  }

  private byte[] executeRound(List<byte[]> broadcastData) {
    if (currentRoundNumber == 3) {
      return proxy.round3(broadcastData);
    } else if (currentRoundNumber == 4) {
      return proxy.round4(broadcastData);
    } else if (currentRoundNumber == 5) {
      return proxy.round5(broadcastData);
    } else { // if (currentRoundNumber == 6)
      return proxy.round6(broadcastData);
    }
  }

  private List<byte[]> getBroadcastMessages(KeyGenMessageRetriever retriever) {
    List<byte[]> messages = new ArrayList<>();
    for (BlockchainPublicKey party : parties) {
      byte[] data = retriever.retrieveBroadcastMessage(party);
      messages.add(Objects.requireNonNullElseGet(data, () -> new byte[0]));
    }
    return messages;
  }

  private InternalStep theSecondRound() {
    return new InternalStep() {
      @Override
      public Step advance(KeyGenMessageRetriever retriever) {
        return round2(retriever);
      }
    };
  }

  private InternalStep anInternalRound() {
    return new InternalStep() {
      @Override
      public Step advance(KeyGenMessageRetriever retriever) {
        return runInternalStep(retriever);
      }
    };
  }

  private InternalStep theLastRound() {
    return new InternalStep() {
      @Override
      public Step advance(KeyGenMessageRetriever retriever) {
        return round7(retriever);
      }
    };
  }
}
