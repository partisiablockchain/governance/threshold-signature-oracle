package com.partisiablockchain.governance.pbcts;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.governance.SignatureCombiningException;
import com.sun.jna.Pointer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class PbctsProxyTest {

  private final AtomicInteger freeCounter = new AtomicInteger();

  private PbctsProxy loadClient(String databaseFilename) {
    return PbctsProxy.load(databaseFilename, "", new byte[32]);
  }

  /**
   * Initialize proxy using memory database with specified library.
   *
   * @param library library to use
   * @return the created proxy
   */
  public static PbctsProxy createProxyWithLibrary(PbctsProxy.Pbcts library) {
    return PbctsProxy.create(
        ":memory:", "", new byte[32], library, PbctsProxy.DEFAULT_POINTER_VACUUM);
  }

  private PbctsProxy createClient(PbctsProxy.Pbcts library) {
    return PbctsProxy.create(
        ":memory:",
        "",
        new byte[32],
        library,
        p -> {
          freeCounter.incrementAndGet();
          PbctsProxy.DEFAULT_POINTER_VACUUM.accept(p);
        });
  }

  @Test
  void createClientSucceeds() {
    PbctsProxy client = PbctsProxy.create(":memory:", "", new byte[32]);
    Assertions.assertThat(client).isNotNull();
    Assertions.assertThat(client.close()).isTrue();
    // client was closed above, so player reference no longer valid
    Assertions.assertThat(client.close()).isFalse();
    assertFreeCalledTimes(0);
  }

  @Test
  void loadClientFromDatabase() {
    String databaseFilename = "src/test/resources/load-client.db";
    PbctsProxy client = loadClient(databaseFilename);
    Assertions.assertThat(client).isNotNull();
    Assertions.assertThat(client.close()).isTrue();
    // cannot create a new player because this database already exists
    Assertions.assertThatThrownBy(() -> PbctsProxy.create(databaseFilename, "", new byte[32]))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Could not obtain a valid player reference");
    // fails to load from non-existing database
    Assertions.assertThatThrownBy(() -> loadClient("missing"))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Could not obtain a valid player reference");
    assertFreeCalledTimes(0);
  }

  private List<PbctsProxy> createClientsWithKeyGenSession(int n) {
    List<PbctsProxy> clients = new ArrayList<>();
    for (int i = 0; i < n; i++) {
      PbctsProxy client = createClient(PbctsProxy.DEFAULT_LIBRARY);
      client.initializeKeyGen(i, n, 5, "sessionId");
      clients.add(client);
    }
    return clients;
  }

  private List<PbctsProxy> createClientsWithLibrary(
      int n, PbctsProxy.Pbcts library, boolean initializeKeyGenSession) {
    List<PbctsProxy> clients = new ArrayList<>();
    for (int i = 0; i < n; i++) {
      PbctsProxy client = createClient(library);
      if (initializeKeyGenSession) {
        client.initializeKeyGen(i, n, 5, "sessionId");
      }
      clients.add(client);
    }
    return clients;
  }

  private List<List<byte[]>> getMessageLists(int size) {
    List<List<byte[]>> lists = new ArrayList<>();
    for (int i = 0; i < size; i++) {
      List<byte[]> inner = new ArrayList<>();
      for (int j = 0; j < size; j++) {
        inner.add(new byte[0]);
      }
      lists.add(inner);
    }
    return lists;
  }

  private List<byte[]> runRound1And2(List<PbctsProxy> clients) {
    int n = clients.size();
    List<List<byte[]>> unicast = getMessageLists(n);
    List<byte[]> broadcast = new ArrayList<>();
    for (int i = 0; i < n; i++) {
      PbctsProxy client = clients.get(i);
      PbctsProxy.Round1Data round1Data = client.round1();
      broadcast.add(round1Data.broadcastMessage);
      for (int j = 0; j < n; j++) {
        unicast.get(j).set(i, round1Data.unicastMessages.get(j));
      }
    }

    List<byte[]> result = new ArrayList<>();
    for (int i = 0; i < n; i++) {
      PbctsProxy client = clients.get(i);
      result.add(client.round2(unicast.get(i), broadcast));
    }

    return result;
  }

  private byte[] runRoundN(int roundNumber, PbctsProxy client, List<byte[]> input) {
    if (roundNumber == 3) {
      return client.round3(input);
    } else if (roundNumber == 4) {
      return client.round4(input);
    } else if (roundNumber == 5) {
      return client.round5(input);
    } else {
      return client.round6(input);
    }
  }

  private List<byte[]> runRoundN(int roundNumber, List<PbctsProxy> clients, List<byte[]> input) {
    List<byte[]> broadcasts = new ArrayList<>();
    for (PbctsProxy client : clients) {
      broadcasts.add(runRoundN(roundNumber, client, input));
    }
    return broadcasts;
  }

  private List<PbctsProxy.KeyGenerationResult> runRound7(
      List<PbctsProxy> clients, List<byte[]> broadcast) {
    List<PbctsProxy.KeyGenerationResult> results = new ArrayList<>();
    for (PbctsProxy client : clients) {
      results.add(client.round7(broadcast));
    }
    return results;
  }

  private List<PbctsProxy.KeyGenerationResult> runKeyGeneration(List<PbctsProxy> clients) {
    List<byte[]> broadcast = runRound1And2(clients);
    broadcast = runRoundN(3, clients, broadcast);
    broadcast = runRoundN(4, clients, broadcast);
    broadcast = runRoundN(5, clients, broadcast);
    broadcast = runRoundN(6, clients, broadcast);
    return runRound7(clients, broadcast);
  }

  @Test
  void keyGen() {
    int numberOfParties = 4;
    List<PbctsProxy> clients = createClientsWithKeyGenSession(numberOfParties);
    List<PbctsProxy.KeyGenerationResult> keyGenerationResults = runKeyGeneration(clients);
    Assertions.assertThat(keyGenerationResults).hasSize(numberOfParties);
    PbctsProxy.KeyGenerationResult first = keyGenerationResults.get(0);
    Assertions.assertThat(first.keyId).isNotEmpty();
    Assertions.assertThat(first.honestPlayers).hasSize(numberOfParties);
    Assertions.assertThat(first.honestPlayers).containsExactly(0L, 1L, 2L, 3L);
    Assertions.assertThat(first.preSignatureIds).hasSize(5);
    for (int i = 1; i < numberOfParties; i++) {
      PbctsProxy.KeyGenerationResult keyGenerationResult = keyGenerationResults.get(i);
      Assertions.assertThat(keyGenerationResult).usingRecursiveComparison().isEqualTo(first);
    }

    PbctsProxy client = clients.get(0);
    List<String> preSignatureIds = client.getPreSignatureIds(first.keyId);
    Assertions.assertThat(preSignatureIds).isEqualTo(first.preSignatureIds);
    BlockchainPublicKey publicKey = client.getPublicKey(first.keyId);
    Assertions.assertThat(publicKey).isNotNull();
    for (int i = 1; i < numberOfParties; i++) {
      PbctsProxy anotherClient = clients.get(i);
      BlockchainPublicKey anotherPublicKey = anotherClient.getPublicKey(first.keyId);
      Assertions.assertThat(publicKey).isEqualTo(anotherPublicKey);
      List<String> moreSignatureIds = anotherClient.getPreSignatureIds(first.keyId);
      Assertions.assertThat(moreSignatureIds).isEqualTo(preSignatureIds);
    }

    // pre-signature count.
    for (PbctsProxy proxy : clients) {
      int count = proxy.preSignatureCount(first.keyId);
      Assertions.assertThat(count).isEqualTo(5);
    }
    assertFreeCalledTimes(numberOfParties * 13);
  }

  @Test
  void deleteStuff() {
    int numberOfParties = 4;
    List<PbctsProxy> clients = createClientsWithKeyGenSession(4);
    List<PbctsProxy.KeyGenerationResult> keyGenerationResults = runKeyGeneration(clients);
    String keyId = keyGenerationResults.get(0).keyId;
    String firstSignatureId = keyGenerationResults.get(0).preSignatureIds.get(0);
    for (int i = 0; i < numberOfParties; i++) {
      PbctsProxy pbctsProxy = clients.get(i);
      int count = pbctsProxy.preSignatureCount(keyId);
      Assertions.assertThat(count).isEqualTo(5);

      Assertions.assertThat(pbctsProxy.deletePreSignature(keyId, firstSignatureId)).isTrue();
      int countAfterDeletion = pbctsProxy.preSignatureCount(keyId);
      Assertions.assertThat(countAfterDeletion).isEqualTo(4);
      List<String> preSignatureIds = pbctsProxy.getPreSignatureIds(keyId);
      Assertions.assertThat(preSignatureIds).doesNotContain(firstSignatureId);
      Assertions.assertThat(pbctsProxy.deletePreSignature(keyId, firstSignatureId)).isFalse();

      Assertions.assertThat(pbctsProxy.deleteAllPreSignatures(keyId)).isTrue();
      Assertions.assertThat(pbctsProxy.preSignatureCount(keyId)).isEqualTo(0);
      Assertions.assertThat(pbctsProxy.getPreSignatureIds(keyId)).isEmpty();

      Assertions.assertThat(pbctsProxy.getPublicKey(keyId)).isNotNull();
      Assertions.assertThat(pbctsProxy.deleteKeyShare(keyId)).isTrue();
      Assertions.assertThatThrownBy(() -> pbctsProxy.getPublicKey(keyId))
          .isInstanceOf(RuntimeException.class)
          .hasMessage("Could not obtain public key");
      Assertions.assertThat(pbctsProxy.deleteKeyShare(keyId)).isFalse();
    }
    assertFreeCalledTimes(numberOfParties * 14);
  }

  private List<byte[]> partiallySign(
      List<PbctsProxy> clients, Hash message, String keyId, String preSigId) {
    List<byte[]> partialSignatures = new ArrayList<>();
    for (PbctsProxy client : clients) {
      partialSignatures.add(client.sign(keyId, preSigId, message, new byte[1]));
    }
    return partialSignatures;
  }

  @Test
  void sign() {
    int numberOfPlayers = 4;
    List<PbctsProxy> clients = createClientsWithKeyGenSession(numberOfPlayers);
    List<PbctsProxy.KeyGenerationResult> keyGenerationResults = runKeyGeneration(clients);

    String keyId = keyGenerationResults.get(0).keyId;
    String preSigId = keyGenerationResults.get(0).preSignatureIds.get(0);
    Hash message = Hash.create(s -> s.writeString("sign me"));

    List<byte[]> partialSignatures = partiallySign(clients, message, keyId, preSigId);

    BlockchainPublicKey publicKey = clients.get(0).getPublicKey(keyId);
    for (PbctsProxy client : clients) {
      PbctsProxy.CombinedSignature combine = client.combine(partialSignatures, message, publicKey);
      Assertions.assertThat(combine.contributors).containsExactly(0L, 1L, 2L, 3L);
      Assertions.assertThat(combine.signature.recoverPublicKey(message)).isEqualTo(publicKey);

      // just to be sure. We still have all pre-signatures
      Assertions.assertThat(client.getPreSignatureIds(keyId)).contains(preSigId);
    }
    assertFreeCalledTimes(61);
  }

  static final class SigningFailsWrapper extends ExtensiblePbctsWrapper {

    @Override
    public PbctsTypes.DataAndError.ByValue pbctsSign(
        long playerReference,
        PbctsTypes.GoString.ByValue keyId,
        PbctsTypes.GoString.ByValue preSignatureId,
        PbctsTypes.GoSlice.ByValue message,
        PbctsTypes.GoSlice.ByValue preSignatureRandomizer) {
      PbctsTypes.DataAndError.ByValue ret = new PbctsTypes.DataAndError.ByValue();
      ret.error = 1;
      return ret;
    }
  }

  @Test
  void signingFails() {
    int numberOfPlayers = 4;
    List<PbctsProxy> clients =
        createClientsWithLibrary(numberOfPlayers, new SigningFailsWrapper(), true);
    List<PbctsProxy.KeyGenerationResult> keyGenerationResults = runKeyGeneration(clients);

    Hash message = Hash.create(s -> s.writeString("sign me"));
    String keyId = keyGenerationResults.get(0).keyId;
    String preSigId = keyGenerationResults.get(0).preSignatureIds.get(0);
    Assertions.assertThatThrownBy(() -> clients.get(0).sign(keyId, preSigId, message, new byte[1]))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Could not sign message");
    assertFreeCalledTimes(numberOfPlayers * 11);
  }

  static final class CombineFailsWrapper extends ExtensiblePbctsWrapper {

    @Override
    public PbctsTypes.CombinedSig.ByValue pbctsCombine(
        PbctsTypes.GoSlice.ByValue partialSignatures) {
      PbctsTypes.CombinedSig.ByValue ret = new PbctsTypes.CombinedSig.ByValue();
      ret.error = 1;
      return ret;
    }
  }

  @Test
  void combineFails() {
    int numberOfPlayers = 4;
    List<PbctsProxy> clients =
        createClientsWithLibrary(numberOfPlayers, new CombineFailsWrapper(), true);
    List<PbctsProxy.KeyGenerationResult> keyGenerationResults = runKeyGeneration(clients);
    String keyId = keyGenerationResults.get(0).keyId;
    String preSigId = keyGenerationResults.get(0).preSignatureIds.get(0);
    Hash message = Hash.create(s -> s.writeString("another message"));
    List<byte[]> partialSignatures = partiallySign(clients, message, keyId, preSigId);

    BlockchainPublicKey publicKey = clients.get(0).getPublicKey(keyId);
    Assertions.assertThatThrownBy(
            () -> clients.get(0).combine(partialSignatures, message, publicKey))
        .isInstanceOf(SignatureCombiningException.class)
        .hasMessage("Could not combine partial signatures");
    assertFreeCalledTimes(49);
  }

  static final class GettersFailWrapper extends ExtensiblePbctsWrapper {

    @Override
    public PbctsTypes.IntAndError.ByValue pbctsPreSignatureCount(
        long playerReference, PbctsTypes.GoString.ByValue keyId) {
      PbctsTypes.IntAndError.ByValue ret = new PbctsTypes.IntAndError.ByValue();
      ret.error = 1;
      return ret;
    }

    @Override
    public PbctsTypes.DataAndError.ByValue pbctsListPreSignatureIds(
        long playerReference, PbctsTypes.GoString.ByValue keyId) {
      PbctsTypes.DataAndError.ByValue ret = new PbctsTypes.DataAndError.ByValue();
      ret.error = 1;
      return ret;
    }
  }

  @Test
  void gettersFail() {
    int numberOfPlayers = 4;
    List<PbctsProxy> clients =
        createClientsWithLibrary(numberOfPlayers, new GettersFailWrapper(), true);
    List<PbctsProxy.KeyGenerationResult> keyGenerationResults = runKeyGeneration(clients);
    String keyId = keyGenerationResults.get(0).keyId;

    Assertions.assertThatThrownBy(() -> clients.get(0).getPreSignatureIds(keyId))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Could not list pre-signatures");

    Assertions.assertThatThrownBy(() -> clients.get(0).preSignatureCount(keyId))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Could not count pre-signatures");
    assertFreeCalledTimes(44);
  }

  static final class SessionInitFailsWrapper extends ExtensiblePbctsWrapper {

    @Override
    public PbctsTypes.Reference.ByValue pbctsNewKeyGenSession(
        long playerReference,
        long playerIndex,
        long playerCount,
        long preSignatureCount,
        PbctsTypes.GoString.ByValue sessionId) {
      PbctsTypes.Reference.ByValue ret = new PbctsTypes.Reference.ByValue();
      ret.error = 1;
      return ret;
    }
  }

  @Test
  void sessionInitFails() {
    int numberOfPlayers = 4;
    List<PbctsProxy> clients =
        createClientsWithLibrary(numberOfPlayers, new SessionInitFailsWrapper(), false);

    Assertions.assertThatThrownBy(
            () -> clients.get(0).initializeKeyGen(0, numberOfPlayers, 10, "sessionId"))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Could not initialize a new key-gen session");
    assertFreeCalledTimes(0);
  }

  static final class Round1FailsWrapper extends ExtensiblePbctsWrapper {

    @Override
    public PbctsTypes.Round1Data.ByValue pbctsRound1(long sessionReference) {
      PbctsTypes.Round1Data.ByValue ret = new PbctsTypes.Round1Data.ByValue();
      ret.error = 1;
      return ret;
    }
  }

  @Test
  void round1Fails() {
    int numberOfPlayers = 4;
    List<PbctsProxy> clients =
        createClientsWithLibrary(numberOfPlayers, new Round1FailsWrapper(), true);

    Assertions.assertThatThrownBy(() -> runKeyGeneration(clients))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Error executing round 1");
    assertFreeCalledTimes(0);
  }

  static final class Round2FailsWrapper extends ExtensiblePbctsWrapper {

    @Override
    public PbctsTypes.DataAndError.ByValue pbctsRound2(
        long sessionReference,
        PbctsTypes.GoSlice.ByValue round1UnicastMessages,
        PbctsTypes.GoSlice.ByValue round1BroadcastMessages) {
      PbctsTypes.DataAndError.ByValue ret = new PbctsTypes.DataAndError.ByValue();
      ret.error = 1;
      return ret;
    }
  }

  @Test
  void round2Fails() {
    int numberOfPlayers = 4;
    List<PbctsProxy> clients =
        createClientsWithLibrary(numberOfPlayers, new Round2FailsWrapper(), true);

    Assertions.assertThatThrownBy(() -> runKeyGeneration(clients))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Error executing round 2");
    assertFreeCalledTimes(numberOfPlayers * 3);
  }

  static final class Round5FailsWrapper extends ExtensiblePbctsWrapper {

    @Override
    public PbctsTypes.DataAndError.ByValue pbctsRound5(
        long sessionReference, PbctsTypes.GoSlice.ByValue round4BroadcastMessages) {
      PbctsTypes.DataAndError.ByValue ret = new PbctsTypes.DataAndError.ByValue();
      ret.error = 1;
      return ret;
    }
  }

  @Test
  void intermediaryRoundFails() {
    int numberOfPlayers = 4;
    List<PbctsProxy> clients =
        createClientsWithLibrary(numberOfPlayers, new Round5FailsWrapper(), true);

    Assertions.assertThatThrownBy(() -> runKeyGeneration(clients))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Error executing round 5");
    assertFreeCalledTimes(numberOfPlayers * 6);
  }

  static final class LastRoundFailsWrapper extends ExtensiblePbctsWrapper {

    @Override
    public PbctsTypes.Round7Data.ByValue pbctsRound7(
        long sessionReference, PbctsTypes.GoSlice.ByValue round6BroadcastMessages) {
      PbctsTypes.Round7Data.ByValue ret = new PbctsTypes.Round7Data.ByValue();
      ret.error = 1;
      return ret;
    }
  }

  @Test
  void lastRoundFails() {
    int numberOfPlayers = 4;
    List<PbctsProxy> clients =
        createClientsWithLibrary(numberOfPlayers, new LastRoundFailsWrapper(), true);

    Assertions.assertThatThrownBy(() -> runKeyGeneration(clients))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Error executing round 7");
    assertFreeCalledTimes(32);
  }

  @Test
  void deleteSession() {
    int numberOfPlayers = 4;
    List<PbctsProxy> clients = createClientsWithKeyGenSession(numberOfPlayers);
    for (PbctsProxy client : clients) {
      Assertions.assertThat(client.deleteSession()).isTrue();
      Assertions.assertThat(client.deleteSession()).isFalse();
    }
    assertFreeCalledTimes(0);
  }

  void assertFreeCalledTimes(int times) {
    Assertions.assertThat(freeCounter.get()).isEqualTo(times);
  }

  @Test
  public void freePointer() {
    AtomicInteger counter = new AtomicInteger();
    Consumer<Pointer> pc = PbctsProxy.freePointer(p -> counter.incrementAndGet());
    pc.accept(Pointer.NULL);
    Assertions.assertThat(counter.get()).isEqualTo(1);
  }

  static final class DeleteAllPreSignaturesFail extends ExtensiblePbctsWrapper {

    @Override
    public boolean pbctsDeleteAllPreSignatures(
        long playerReference, PbctsTypes.GoString.ByValue keyId) {
      return false;
    }
  }

  @Test
  void deletePreSignatureAndKeyShareFails() {
    // not actually sure under which conditions calls to deleteAllPreSignatures might fail in
    // practice.
    List<PbctsProxy> clients = createClientsWithLibrary(4, new DeleteAllPreSignaturesFail(), true);
    List<PbctsProxy.KeyGenerationResult> keyGenerationResults = runKeyGeneration(clients);
    String keyId = keyGenerationResults.get(0).keyId;
    Assertions.assertThat(clients.get(0).deleteAllPreSignatures(keyId)).isFalse();
  }

  static final class ResumeKeyGenFails extends ExtensiblePbctsWrapper {

    @Override
    public PbctsTypes.Reference.ByValue pbctsResumeKeyGenSession(
        long playerReference, PbctsTypes.GoString.ByValue sessionId) {
      PbctsTypes.Reference.ByValue ret = new PbctsTypes.Reference.ByValue();
      ret.error = 1;
      return ret;
    }
  }

  @Test
  void resumeKeyGenFails() {
    PbctsProxy client = createClient(new ResumeKeyGenFails());
    Assertions.assertThatThrownBy(() -> client.resumeKeyGen("sessionId"))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Could not resume key-gen session for session: sessionId");
  }

  static final class ShutdownFails extends ExtensiblePbctsWrapper {

    @Override
    public boolean pbctsFlushKeyGenSessionCache(long playerReference) {
      return false;
    }
  }

  @Test
  void shutdownFails() {
    PbctsProxy client = createClient(new ShutdownFails());
    Assertions.assertThatThrownBy(client::shutdown)
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Error encountered while flushing key-gen session cache");
  }

  /**
   * A wrapper for the external pbcts library. Can be used to test failure cases that occur outside
   * of java.
   */
  public abstract static class ExtensiblePbctsWrapper implements PbctsProxy.Pbcts {

    private final PbctsLibraryWrapper wrapper = new PbctsLibraryWrapper();

    @Override
    public PbctsTypes.DataAndError.ByValue pbctsSign(
        long playerReference,
        PbctsTypes.GoString.ByValue keyId,
        PbctsTypes.GoString.ByValue preSignatureId,
        PbctsTypes.GoSlice.ByValue message,
        PbctsTypes.GoSlice.ByValue preSignatureRandomizer) {
      return wrapper.pbctsSign(
          playerReference, keyId, preSignatureId, message, preSignatureRandomizer);
    }

    @Override
    public PbctsTypes.CombinedSig.ByValue pbctsCombine(
        PbctsTypes.GoSlice.ByValue partialSignatures) {
      return wrapper.pbctsCombine(partialSignatures);
    }

    @Override
    public boolean pbctsDeleteKeyShare(long playerReference, PbctsTypes.GoString.ByValue keyId) {
      return wrapper.pbctsDeleteKeyShare(playerReference, keyId);
    }

    @Override
    public boolean pbctsDeletePreSignature(
        long playerReference,
        PbctsTypes.GoString.ByValue keyId,
        PbctsTypes.GoString.ByValue preSignatureId) {
      return wrapper.pbctsDeletePreSignature(playerReference, keyId, preSignatureId);
    }

    @Override
    public boolean pbctsDeleteAllPreSignatures(
        long playerReference, PbctsTypes.GoString.ByValue keyId) {
      return wrapper.pbctsDeleteAllPreSignatures(playerReference, keyId);
    }

    @Override
    public PbctsTypes.IntAndError.ByValue pbctsPreSignatureCount(
        long playerReference, PbctsTypes.GoString.ByValue keyId) {
      return wrapper.pbctsPreSignatureCount(playerReference, keyId);
    }

    @Override
    public PbctsTypes.DataAndError.ByValue pbctsListPreSignatureIds(
        long playerReference, PbctsTypes.GoString.ByValue keyId) {
      return wrapper.pbctsListPreSignatureIds(playerReference, keyId);
    }

    @Override
    public PbctsTypes.Round1Data.ByValue pbctsRound1(long sessionReference) {
      return wrapper.pbctsRound1(sessionReference);
    }

    @Override
    public PbctsTypes.DataAndError.ByValue pbctsRound2(
        long sessionReference,
        PbctsTypes.GoSlice.ByValue round1UnicastMessages,
        PbctsTypes.GoSlice.ByValue round1BroadcastMessages) {
      return wrapper.pbctsRound2(sessionReference, round1UnicastMessages, round1BroadcastMessages);
    }

    @Override
    public PbctsTypes.DataAndError.ByValue pbctsRound3(
        long sessionReference, PbctsTypes.GoSlice.ByValue round2BroadcastMessages) {
      return wrapper.pbctsRound3(sessionReference, round2BroadcastMessages);
    }

    @Override
    public PbctsTypes.DataAndError.ByValue pbctsRound4(
        long sessionReference, PbctsTypes.GoSlice.ByValue round3BroadcastMessages) {
      return wrapper.pbctsRound4(sessionReference, round3BroadcastMessages);
    }

    @Override
    public PbctsTypes.DataAndError.ByValue pbctsRound5(
        long sessionReference, PbctsTypes.GoSlice.ByValue round4BroadcastMessages) {
      return wrapper.pbctsRound5(sessionReference, round4BroadcastMessages);
    }

    @Override
    public PbctsTypes.DataAndError.ByValue pbctsRound6(
        long sessionReference, PbctsTypes.GoSlice.ByValue round5BroadcastMessages) {
      return wrapper.pbctsRound6(sessionReference, round5BroadcastMessages);
    }

    @Override
    public PbctsTypes.Round7Data.ByValue pbctsRound7(
        long sessionReference, PbctsTypes.GoSlice.ByValue round6BroadcastMessages) {
      return wrapper.pbctsRound7(sessionReference, round6BroadcastMessages);
    }

    @Override
    public boolean pbctsClose(long playerReference) {
      return wrapper.pbctsClose(playerReference);
    }

    @Override
    public boolean pbctsDeleteSession(long sessionReference) {
      return wrapper.pbctsDeleteSession(sessionReference);
    }

    @Override
    public PbctsTypes.Reference.ByValue pbctsNew(
        PbctsTypes.GoString.ByValue databaseFilename,
        PbctsTypes.GoString.ByValue databaseBackupFilename,
        PbctsTypes.GoSlice.ByValue databaseKey) {
      return wrapper.pbctsNew(databaseFilename, databaseBackupFilename, databaseKey);
    }

    @Override
    public PbctsTypes.Reference.ByValue pbctsLoad(
        PbctsTypes.GoString.ByValue databaseFilename,
        PbctsTypes.GoString.ByValue databaseBackupFilename,
        PbctsTypes.GoSlice.ByValue databaseKey) {
      return wrapper.pbctsLoad(databaseFilename, databaseBackupFilename, databaseKey);
    }

    @Override
    public PbctsTypes.Reference.ByValue pbctsNewKeyGenSession(
        long playerReference,
        long playerIndex,
        long playerCount,
        long preSignatureCount,
        PbctsTypes.GoString.ByValue sessionId) {
      return wrapper.pbctsNewKeyGenSession(
          playerReference, playerIndex, playerCount, preSignatureCount, sessionId);
    }

    @Override
    public PbctsTypes.Reference.ByValue pbctsResumeKeyGenSession(
        long playerReference, PbctsTypes.GoString.ByValue sessionId) {
      return wrapper.pbctsResumeKeyGenSession(playerReference, sessionId);
    }

    @Override
    public boolean pbctsFlushKeyGenSessionCache(long playerReference) {
      return wrapper.pbctsFlushKeyGenSessionCache(playerReference);
    }

    @Override
    public PbctsTypes.DataAndError.ByValue pbctsPublicKey(
        long playerReference, PbctsTypes.GoString.ByValue keyId) {
      return wrapper.pbctsPublicKey(playerReference, keyId);
    }
  }
}
