package com.partisiablockchain.governance.pbcts;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

final class PbctsLibraryWrapper implements PbctsProxy.Pbcts {

  private static final PbctsProxy.Pbcts library = PbctsProxy.DEFAULT_LIBRARY;

  @Override
  public PbctsTypes.Reference.ByValue pbctsNew(
      PbctsTypes.GoString.ByValue databaseFilename,
      PbctsTypes.GoString.ByValue databaseBackupFilename,
      PbctsTypes.GoSlice.ByValue databaseKey) {
    return library.pbctsNew(databaseFilename, databaseBackupFilename, databaseKey);
  }

  @Override
  public PbctsTypes.Reference.ByValue pbctsLoad(
      PbctsTypes.GoString.ByValue databaseFilename,
      PbctsTypes.GoString.ByValue databaseBackupFilename,
      PbctsTypes.GoSlice.ByValue databaseKey) {
    return library.pbctsLoad(databaseFilename, databaseBackupFilename, databaseKey);
  }

  @Override
  public PbctsTypes.Reference.ByValue pbctsNewKeyGenSession(
      long playerReference,
      long playerIndex,
      long playerCount,
      long preSignatureCount,
      PbctsTypes.GoString.ByValue sessionId) {
    return library.pbctsNewKeyGenSession(
        playerReference, playerIndex, playerCount, preSignatureCount, sessionId);
  }

  @Override
  public PbctsTypes.Reference.ByValue pbctsResumeKeyGenSession(
      long playerReference, PbctsTypes.GoString.ByValue sessionId) {
    return library.pbctsResumeKeyGenSession(playerReference, sessionId);
  }

  @Override
  public boolean pbctsFlushKeyGenSessionCache(long playerReference) {
    return library.pbctsFlushKeyGenSessionCache(playerReference);
  }

  @Override
  public PbctsTypes.DataAndError.ByValue pbctsPublicKey(
      long playerReference, PbctsTypes.GoString.ByValue keyId) {
    return library.pbctsPublicKey(playerReference, keyId);
  }

  @Override
  public PbctsTypes.DataAndError.ByValue pbctsSign(
      long playerReference,
      PbctsTypes.GoString.ByValue keyId,
      PbctsTypes.GoString.ByValue preSignatureId,
      PbctsTypes.GoSlice.ByValue message,
      PbctsTypes.GoSlice.ByValue preSignatureRandomizer) {
    return library.pbctsSign(
        playerReference, keyId, preSignatureId, message, preSignatureRandomizer);
  }

  @Override
  public PbctsTypes.CombinedSig.ByValue pbctsCombine(PbctsTypes.GoSlice.ByValue partialSignatures) {
    return library.pbctsCombine(partialSignatures);
  }

  @Override
  public boolean pbctsDeleteKeyShare(long playerReference, PbctsTypes.GoString.ByValue keyId) {
    return library.pbctsDeleteKeyShare(playerReference, keyId);
  }

  @Override
  public boolean pbctsDeletePreSignature(
      long playerReference,
      PbctsTypes.GoString.ByValue keyId,
      PbctsTypes.GoString.ByValue preSignatureId) {
    return library.pbctsDeletePreSignature(playerReference, keyId, preSignatureId);
  }

  @Override
  public boolean pbctsDeleteAllPreSignatures(
      long playerReference, PbctsTypes.GoString.ByValue keyId) {
    return library.pbctsDeleteAllPreSignatures(playerReference, keyId);
  }

  @Override
  public PbctsTypes.IntAndError.ByValue pbctsPreSignatureCount(
      long playerReference, PbctsTypes.GoString.ByValue keyId) {
    return library.pbctsPreSignatureCount(playerReference, keyId);
  }

  @Override
  public PbctsTypes.DataAndError.ByValue pbctsListPreSignatureIds(
      long playerReference, PbctsTypes.GoString.ByValue keyId) {
    return library.pbctsListPreSignatureIds(playerReference, keyId);
  }

  @Override
  public PbctsTypes.Round1Data.ByValue pbctsRound1(long sessionReference) {
    return library.pbctsRound1(sessionReference);
  }

  @Override
  public PbctsTypes.DataAndError.ByValue pbctsRound2(
      long sessionReference,
      PbctsTypes.GoSlice.ByValue round1UnicastMessages,
      PbctsTypes.GoSlice.ByValue round1BroadcastMessages) {
    return library.pbctsRound2(sessionReference, round1UnicastMessages, round1BroadcastMessages);
  }

  @Override
  public PbctsTypes.DataAndError.ByValue pbctsRound3(
      long sessionReference, PbctsTypes.GoSlice.ByValue round2BroadcastMessages) {
    return library.pbctsRound3(sessionReference, round2BroadcastMessages);
  }

  @Override
  public PbctsTypes.DataAndError.ByValue pbctsRound4(
      long sessionReference, PbctsTypes.GoSlice.ByValue round3BroadcastMessages) {
    return library.pbctsRound4(sessionReference, round3BroadcastMessages);
  }

  @Override
  public PbctsTypes.DataAndError.ByValue pbctsRound5(
      long sessionReference, PbctsTypes.GoSlice.ByValue round4BroadcastMessages) {
    return library.pbctsRound5(sessionReference, round4BroadcastMessages);
  }

  @Override
  public PbctsTypes.DataAndError.ByValue pbctsRound6(
      long sessionReference, PbctsTypes.GoSlice.ByValue round5BroadcastMessages) {
    return library.pbctsRound6(sessionReference, round5BroadcastMessages);
  }

  @Override
  public PbctsTypes.Round7Data.ByValue pbctsRound7(
      long sessionReference, PbctsTypes.GoSlice.ByValue round6BroadcastMessages) {
    return library.pbctsRound7(sessionReference, round6BroadcastMessages);
  }

  @Override
  public boolean pbctsClose(long playerReference) {
    return library.pbctsClose(playerReference);
  }

  @Override
  public boolean pbctsDeleteSession(long sessionReference) {
    return library.pbctsDeleteSession(sessionReference);
  }
}
