package com.partisiablockchain.governance.pbcts;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class PbctsTypesTest {

  @Test
  void types() {
    PbctsTypes.IntAndError.ByValue byValue = new PbctsTypes.IntAndError.ByValue();
    Assertions.assertThat(byValue).isNotNull();

    PbctsTypes.Round1Data.ByValue byValue1 = new PbctsTypes.Round1Data.ByValue();
    Assertions.assertThat(byValue1).isNotNull();

    PbctsTypes.Round7Data.ByValue byValue2 = new PbctsTypes.Round7Data.ByValue();
    Assertions.assertThat(byValue2).isNotNull();
  }
}
