package com.partisiablockchain.governance.pbcts;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Curve;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import org.junit.jupiter.api.Test;

final class SignatureHelperTest {

  @Test
  void findRecoveryId() {
    KeyPair keyPair = new KeyPair();
    byte[] message = new byte[32];
    Hash messageHash = Hash.create(out -> out.write(message));
    Signature signature = keyPair.sign(messageHash);

    Signature withRecoveryId =
        SignatureHelper.findRecoveryId(
            messageHash,
            signature.getR(),
            signature.getS(),
            BlockchainPublicKey.fromEncodedEcPoint(Curve.CURVE.getG().getEncoded(true)));
    assertThat(withRecoveryId).isNull();
  }
}
