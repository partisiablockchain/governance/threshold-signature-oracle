package com.partisiablockchain.governance;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.governance.pbcts.PbctsProxyTest;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

/** Test. */
public final class PbctsKeyGenTest {

  private static final ThresholdSessionId sessionId = new ThresholdSessionId(0, 0);
  private static final String databaseName = ":memory:";
  private static final String databaseBackupName = "";
  private static final byte[] databaseKey = new byte[32];

  private static final int preSignatureCount = 5;

  private List<BlockchainPublicKey> parties;
  private List<MessageStorage> storages;

  static List<BlockchainPublicKey> createParties(int n) {
    List<BlockchainPublicKey> parties = new ArrayList<>();
    for (int i = 0; i < n; i++) {
      BlockchainPublicKey address = new KeyPair(BigInteger.valueOf(42 + i)).getPublic();
      parties.add(address);
    }
    return parties;
  }

  static List<MessageStorage> createStorages(List<BlockchainPublicKey> parties) {
    List<MessageStorage> storages = new ArrayList<>();
    for (int i = 0; i < parties.size(); i++) {
      storages.add(new DummyStorage(sessionId));
    }
    return storages;
  }

  private void setupFourPartiesAndStorages() {
    parties = createParties(4);
    storages = createStorages(parties);
  }

  static List<ThresholdKeyGenProtocol> createProtocols(
      ThresholdSessionId sessionId,
      List<BlockchainPublicKey> parties,
      List<MessageStorage> storages,
      int preSignatureCount,
      String databaseName,
      String databaseBackupName,
      byte[] databaseKey) {
    List<ThresholdKeyGenProtocol> protocols = new ArrayList<>();
    for (int i = 0; i < parties.size(); i++) {
      BlockchainPublicKey party = parties.get(i);
      MessageStorage messageStorage = storages.get(i);
      protocols.add(
          PbctsKeyGen.create(
              sessionId,
              parties,
              party,
              messageStorage,
              preSignatureCount,
              databaseName,
              databaseBackupName,
              databaseKey));
    }
    return protocols;
  }

  private List<ThresholdKeyGenProtocol> getProtocols() {
    return createProtocols(
        sessionId,
        parties,
        storages,
        preSignatureCount,
        databaseName,
        databaseBackupName,
        databaseKey);
  }

  static List<ThresholdKeyGenProtocol.Step> init(List<ThresholdKeyGenProtocol> protocols) {
    List<ThresholdKeyGenProtocol.Step> steps = new ArrayList<>();
    for (ThresholdKeyGenProtocol protocol : protocols) {
      steps.add(protocol.init());
    }
    return steps;
  }

  static List<KeyGenMessageRetriever> getRetrievers(List<BlockchainPublicKey> parties) {
    List<KeyGenMessageRetriever> retrievers = new ArrayList<>();
    for (int i = 0; i < parties.size(); i++) {
      retrievers.add(Mockito.mock(KeyGenMessageRetriever.class));
    }
    return retrievers;
  }

  private List<KeyGenMessageRetriever> getRetrievers() {
    return getRetrievers(parties);
  }

  @Test
  void runKeyGenerationSucceeds() {
    setupFourPartiesAndStorages();
    List<ThresholdKeyGenProtocol> protocols = getProtocols();
    List<ThresholdKeyGenProtocol.Step> init = init(protocols);
    List<KeyGenMessageRetriever> retrievers = getRetrievers();

    // round 2
    assertRoundNumberIs(init, 2);
    prepareUnicastMessages(retrievers);
    prepareBroadcastMessages(2, retrievers);
    List<ThresholdKeyGenProtocol.Step> data2 = executeRound(retrievers, init);

    // round 3
    assertRoundNumberIs(data2, 3);
    prepareBroadcastMessages(3, retrievers);
    List<ThresholdKeyGenProtocol.Step> data3 = executeRound(retrievers, data2);

    // round 4
    assertRoundNumberIs(data3, 4);
    prepareBroadcastMessages(4, retrievers);
    List<ThresholdKeyGenProtocol.Step> data4 = executeRound(retrievers, data3);

    // round 5
    assertRoundNumberIs(data3, 5);
    prepareBroadcastMessages(5, retrievers);
    List<ThresholdKeyGenProtocol.Step> data5 = executeRound(retrievers, data4);

    // round 6
    assertRoundNumberIs(data3, 6);
    prepareBroadcastMessages(6, retrievers);
    List<ThresholdKeyGenProtocol.Step> data6 = executeRound(retrievers, data5);

    // final round
    assertRoundNumberIs(data3, 7);
    prepareBroadcastMessages(7, retrievers);
    List<ThresholdKeyGenProtocol.Step> finalSteps = executeRound(retrievers, data6);

    List<ThresholdSignatureProvider> providers = finalizeProtocols(finalSteps);
    assertKeyGenResultIsConsistent(providers);
  }

  @Test
  void runKeyGenWithResumingPartyInRound2() throws IOException {
    setupFourPartiesAndStorages();
    List<ThresholdKeyGenProtocol> protocols = getProtocols();
    Path dir = Files.createTempDirectory("runKeyGenWithResumingPartyInRound2");
    String databaseName = dir + "/database.db";
    protocols.set(0, createProtocolWithDatabase(databaseName, 0));
    List<ThresholdKeyGenProtocol.Step> init = init(protocols);
    List<KeyGenMessageRetriever> retrievers = getRetrievers();

    // round 2
    assertRoundNumberIs(init, 2);
    prepareUnicastMessages(retrievers);
    prepareBroadcastMessages(2, retrievers);
    // run parties 1 to 3
    List<ThresholdKeyGenProtocol.Step> dataPartial =
        executeRound(retrievers.subList(1, 4), init.subList(1, 4));
    PbctsKeyGen party0 =
        PbctsKeyGen.load(
            sessionId,
            parties,
            parties.get(0),
            storages.get(0),
            preSignatureCount,
            databaseName,
            databaseBackupName,
            databaseKey);
    ThresholdKeyGenProtocol.Step resume = party0.resume(2);
    List<ThresholdKeyGenProtocol.Step> steps =
        executeRound(List.of(retrievers.get(0)), List.of(resume));

    List<ThresholdKeyGenProtocol.Step> data =
        List.of(steps.get(0), dataPartial.get(0), dataPartial.get(1), dataPartial.get(2));

    for (int i = 3; i < 8; i++) {
      assertRoundNumberIs(data, i);
      prepareBroadcastMessages(i, retrievers);
      data = executeRound(retrievers, data);
    }

    List<ThresholdSignatureProvider> signatureProviders = finalizeProtocols(data);
    assertKeyGenResultIsConsistent(signatureProviders);
  }

  @Test
  void runKeyGenWithResumingPartyInInternalRound() throws IOException {
    setupFourPartiesAndStorages();
    List<ThresholdKeyGenProtocol> protocols = getProtocols();
    Path dir = Files.createTempDirectory("runKeyGenWithResumingPartyInInternalRound");
    String databaseName = dir + "/database.db";
    protocols.set(0, createProtocolWithDatabase(databaseName, 0));
    List<ThresholdKeyGenProtocol.Step> init = init(protocols);
    List<KeyGenMessageRetriever> retrievers = getRetrievers();

    // round 2
    assertRoundNumberIs(init, 2);
    prepareUnicastMessages(retrievers);
    prepareBroadcastMessages(2, retrievers);
    List<ThresholdKeyGenProtocol.Step> data = executeRound(retrievers, init);

    assertRoundNumberIs(data, 3);
    prepareBroadcastMessages(3, retrievers);
    data = executeRound(retrievers, data);

    assertRoundNumberIs(data, 4);
    // run parties 1 to 3
    prepareBroadcastMessages(4, retrievers);
    List<ThresholdKeyGenProtocol.Step> partial =
        executeRound(retrievers.subList(1, 4), data.subList(1, 4));
    protocols.get(0).shutdown();
    PbctsKeyGen party0 =
        PbctsKeyGen.load(
            sessionId,
            parties,
            parties.get(0),
            storages.get(0),
            preSignatureCount,
            databaseName,
            databaseBackupName,
            databaseKey);
    ThresholdKeyGenProtocol.Step resume = party0.resume(4);
    List<ThresholdKeyGenProtocol.Step> steps =
        executeRound(List.of(retrievers.get(0)), List.of(resume));

    data = List.of(steps.get(0), partial.get(0), partial.get(1), partial.get(2));

    for (int i = 5; i < 8; i++) {
      assertRoundNumberIs(data, i);
      prepareBroadcastMessages(i, retrievers);
      data = executeRound(retrievers, data);
    }

    List<ThresholdSignatureProvider> signatureProviders = finalizeProtocols(data);
    assertKeyGenResultIsConsistent(signatureProviders);
  }

  private ThresholdKeyGenProtocol createProtocolWithDatabase(String databaseName, int id) {
    BlockchainPublicKey publicKey = parties.get(id);
    MessageStorage messageStorage = storages.get(id);
    return PbctsKeyGen.create(
        sessionId,
        parties,
        publicKey,
        messageStorage,
        preSignatureCount,
        databaseName,
        databaseBackupName,
        databaseKey);
  }

  private void assertKeyGenResultIsConsistent(List<ThresholdSignatureProvider> providers) {
    ThresholdSignatureProvider first = providers.get(0);
    ThresholdPublicKey publicKey = first.getPublicKey();
    Assertions.assertThat(publicKey.getPublicKeyId()).isNotNull();
    Assertions.assertThat(publicKey.getPublicKeyId()).isNotEmpty();
    Assertions.assertThat(publicKey.getPublicKey()).isNotNull();
    Assertions.assertThat(first.getHonestParties()).usingRecursiveComparison().isEqualTo(parties);
    for (int i = 1; i < parties.size(); i++) {
      ThresholdSignatureProvider provider = providers.get(i);
      Assertions.assertThat(provider.getRemainingSignatures()).isEqualTo(preSignatureCount);
      ThresholdPublicKey otherPublicKey = provider.getPublicKey();
      Assertions.assertThat(otherPublicKey).isNotNull();
      Assertions.assertThat(otherPublicKey.getPublicKey()).isEqualTo(publicKey.getPublicKey());
      Assertions.assertThat(otherPublicKey.getPublicKeyId()).isEqualTo(publicKey.getPublicKeyId());
      Assertions.assertThat(provider.getSignatureIds())
          .usingRecursiveComparison()
          .isEqualTo(first.getSignatureIds());
      Assertions.assertThat(provider.getHonestParties())
          .usingRecursiveComparison()
          .isEqualTo(parties);
    }
  }

  @Test
  void shutdownCalledCorrectly() {
    setupFourPartiesAndStorages();
    AtomicInteger counter = new AtomicInteger(0);
    PbctsKeyGen pbctsKeyGen =
        new PbctsKeyGen(
            sessionId,
            parties,
            parties.get(0),
            storages.get(0),
            1,
            databaseName,
            databaseBackupName,
            databaseKey,
            false,
            p -> counter.incrementAndGet());
    pbctsKeyGen.shutdown();
    Assertions.assertThat(counter.get()).isEqualTo(1);
  }

  static final class ProxyWithFailingCloseOrShutdown extends PbctsProxyTest.ExtensiblePbctsWrapper {

    private final boolean closeShouldFail;
    private final boolean flushShouldFail;

    ProxyWithFailingCloseOrShutdown(boolean closeShouldFail, boolean flushShouldFail) {
      this.closeShouldFail = closeShouldFail;
      this.flushShouldFail = flushShouldFail;
    }

    @Override
    public boolean pbctsClose(long playerReference) {
      if (closeShouldFail) {
        throw new RuntimeException("close failed");
      }
      return true;
    }

    @Override
    public boolean pbctsFlushKeyGenSessionCache(long playerReference) {
      return !flushShouldFail;
    }
  }

  @Test
  void shutdownHook() {
    final var proxy =
        PbctsProxyTest.createProxyWithLibrary(new ProxyWithFailingCloseOrShutdown(true, false));
    Assertions.assertThatThrownBy(() -> PbctsKeyGen.DEFAULT_SHUTDOWN_HOOK.accept(proxy))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("close failed");

    final var anotherProxy =
        PbctsProxyTest.createProxyWithLibrary(new ProxyWithFailingCloseOrShutdown(false, true));
    Assertions.assertThatThrownBy(() -> PbctsKeyGen.DEFAULT_SHUTDOWN_HOOK.accept(anotherProxy))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Error encountered while flushing key-gen session cache");
  }

  @Test
  void keyGenWithMissingUnicastMessage() {
    setupFourPartiesAndStorages();
    List<ThresholdKeyGenProtocol> protocols = getProtocols();
    final List<ThresholdKeyGenProtocol.Step> init = init(protocols);
    List<KeyGenMessageRetriever> retrievers = getRetrievers();

    prepareUnicastMessages(retrievers);
    // even if a party misses a unicast message, it can still arrive at the correct key in the end.
    prepareUnicastMessage(retrievers.get(0), null, parties.get(1));
    prepareBroadcastMessages(2, retrievers);
    List<ThresholdKeyGenProtocol.Step> steps = executeRound(retrievers, init);

    steps = runToEndFromStep3(steps, retrievers);

    List<ThresholdSignatureProvider> signatureProviders = finalizeProtocols(steps);
    assertKeyGenResultIsConsistent(signatureProviders);
  }

  @Test
  void keyGenWithMissingBroadcastMessages() {
    setupFourPartiesAndStorages();
    List<ThresholdKeyGenProtocol> protocols = getProtocols();
    List<ThresholdKeyGenProtocol.Step> init = init(protocols);
    List<KeyGenMessageRetriever> retrievers = getRetrievers();

    prepareUnicastMessages(retrievers);
    // from here the last party is considered "dead".
    retrievers = retrievers.subList(0, 3);
    init = init.subList(0, 3);
    parties = parties.subList(0, 3);
    storages = storages.subList(0, 3);
    prepareBroadcastMessages(2, retrievers);
    List<ThresholdKeyGenProtocol.Step> steps = executeRound(retrievers, init);

    steps = runToEndFromStep3(steps, retrievers);

    List<ThresholdSignatureProvider> signatureProviders = finalizeProtocols(steps);
    for (ThresholdSignatureProvider signatureProvider : signatureProviders) {
      Assertions.assertThat(signatureProvider.getHonestParties())
          .usingRecursiveComparison()
          .isEqualTo(parties);
    }
  }

  private List<ThresholdKeyGenProtocol.Step> runToEndFromStep3(
      List<ThresholdKeyGenProtocol.Step> steps, List<KeyGenMessageRetriever> retrievers) {
    List<ThresholdKeyGenProtocol.Step> result = steps;
    for (int i = 3; i <= 7; i++) {
      prepareBroadcastMessages(i, retrievers);
      result = executeRound(retrievers, result);
    }
    return result;
  }

  @Test
  void load() {
    setupFourPartiesAndStorages();
    DummyStorage storage = new DummyStorage(sessionId);
    PbctsKeyGen load =
        PbctsKeyGen.load(
            sessionId,
            parties,
            parties.get(0),
            storage,
            preSignatureCount,
            "src/test/resources/load-client.db",
            databaseBackupName,
            databaseKey);
    Assertions.assertThat(load.getRoundNumber()).isEqualTo(1);
    ThresholdKeyGenProtocol.Step init = load.init();
    Assertions.assertThat(init.roundNumber()).isEqualTo(2);
  }

  @Test
  void sessionIdToString() {
    setupFourPartiesAndStorages();
    List<ThresholdKeyGenProtocol> protocols = getProtocols();
    PbctsKeyGen pbctsKeyGen = (PbctsKeyGen) protocols.get(0);
    String sessionIdString = pbctsKeyGen.sessionIdToString();
    Assertions.assertThat(sessionIdString)
        .isEqualTo(
            Hash.create(
                    s -> {
                      s.writeString("LargeOracleKeyGenSession");
                      s.writeInt(0);
                      s.writeInt(0);
                    })
                .toString());
  }

  private void assertRoundNumberIs(List<ThresholdKeyGenProtocol.Step> steps, int roundNumber) {
    for (ThresholdKeyGenProtocol.Step step : steps) {
      Assertions.assertThat(step.roundNumber()).isEqualTo(roundNumber);
    }
  }

  static List<ThresholdKeyGenProtocol.Step> executeRound(
      List<KeyGenMessageRetriever> retrievers, List<ThresholdKeyGenProtocol.Step> steps) {
    List<ThresholdKeyGenProtocol.Step> nextSteps = new ArrayList<>();
    for (int i = 0; i < steps.size(); i++) {
      ThresholdKeyGenProtocol.Step step = steps.get(i);
      KeyGenMessageRetriever keyGenMessageRetriever = retrievers.get(i);
      if (step != null) {
        ThresholdKeyGenProtocol.Step next = runInternalStep(step, keyGenMessageRetriever);
        nextSteps.add(next);
      } else {
        nextSteps.add(null);
      }
    }
    return nextSteps;
  }

  static ThresholdKeyGenProtocol.Step runInternalStep(
      ThresholdKeyGenProtocol.Step step, KeyGenMessageRetriever retriever) {
    Assertions.assertThat(step).isInstanceOf(ThresholdKeyGenProtocol.InternalStep.class);
    return ((ThresholdKeyGenProtocol.InternalStep) step).advance(retriever);
  }

  static List<ThresholdSignatureProvider> finalizeProtocols(
      List<ThresholdKeyGenProtocol.Step> steps) {
    List<ThresholdSignatureProvider> signers = new ArrayList<>();
    for (ThresholdKeyGenProtocol.Step step : steps) {
      if (step != null) {
        signers.add(finalizeProtocol(step));
      }
    }
    return signers;
  }

  static ThresholdSignatureProvider finalizeProtocol(ThresholdKeyGenProtocol.Step step) {
    Assertions.assertThat(step).isInstanceOf(ThresholdKeyGenProtocol.FinalStep.class);
    return ((ThresholdKeyGenProtocol.FinalStep) step).finish();
  }

  static void prepareBroadcastMessages(
      int roundNumber,
      List<BlockchainPublicKey> parties,
      List<MessageStorage> storages,
      List<KeyGenMessageRetriever> retrievers) {
    for (int i = 0; i < parties.size(); i++) {
      BlockchainPublicKey sender = parties.get(i);
      DummyStorage storage = (DummyStorage) storages.get(i);
      for (KeyGenMessageRetriever retriever : retrievers) {
        Mockito.when(retriever.retrieveBroadcastMessage(Mockito.eq(sender)))
            .thenReturn(storage.getMyBroadcastMessage(roundNumber));
      }
    }
  }

  private void prepareBroadcastMessages(int roundNumber, List<KeyGenMessageRetriever> retrievers) {
    prepareBroadcastMessages(roundNumber, parties, storages, retrievers);
  }

  private void prepareUnicastMessages(List<KeyGenMessageRetriever> retrievers) {
    prepareUnicastMessages(sessionId, parties, retrievers, storages);
  }

  static void prepareUnicastMessages(
      ThresholdSessionId sessionId,
      List<BlockchainPublicKey> parties,
      List<KeyGenMessageRetriever> retrievers,
      List<MessageStorage> storages) {
    for (int i = 0; i < parties.size(); i++) {
      BlockchainPublicKey receiver = parties.get(i);
      KeyGenMessageRetriever receiverRetriever = retrievers.get(i);
      for (int j = 0; j < parties.size(); j++) {
        MessageStorage messageStorage = storages.get(j);
        BlockchainPublicKey sender = parties.get(j);
        byte[] message = messageStorage.getRoundUnicastMessage(sessionId, 2, receiver);
        prepareUnicastMessage(receiverRetriever, message, sender);
      }
    }
  }

  static void prepareUnicastMessage(
      KeyGenMessageRetriever receiverRetriever, byte[] message, BlockchainPublicKey sender) {
    Mockito.when(receiverRetriever.retrieveUnicastMessage(Mockito.eq(sender))).thenReturn(message);
  }
}
