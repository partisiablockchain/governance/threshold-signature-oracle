package com.partisiablockchain.governance;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.LongStream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

/** Test. */
public final class PbctsSignerTest {

  private static final ThresholdSessionId sessionId = new ThresholdSessionId(0, 0);
  private static final String databaseName = ":memory:";
  private static final String databaseBackupName = "";
  private static final byte[] databaseKey = new byte[32];
  private static final byte[] dummyRandomizer = new byte[1]; // just can't be empty.

  private static final int preSignatureCount = 5;

  private List<BlockchainPublicKey> parties;
  private List<MessageStorage> storages;

  private List<ThresholdSignatureProvider> createSigners(int n) {
    parties = PbctsKeyGenTest.createParties(n);
    storages = PbctsKeyGenTest.createStorages(parties);
    List<ThresholdKeyGenProtocol> protocols =
        PbctsKeyGenTest.createProtocols(
            sessionId,
            parties,
            storages,
            preSignatureCount,
            databaseName,
            databaseBackupName,
            databaseKey);

    List<ThresholdKeyGenProtocol.Step> steps = PbctsKeyGenTest.init(protocols);
    List<KeyGenMessageRetriever> retrievers = PbctsKeyGenTest.getRetrievers(parties);

    PbctsKeyGenTest.prepareUnicastMessages(sessionId, parties, retrievers, storages);
    PbctsKeyGenTest.prepareBroadcastMessages(2, parties, storages, retrievers);

    for (int i = 3; i <= 7; i++) {
      steps = PbctsKeyGenTest.executeRound(retrievers, steps);
      PbctsKeyGenTest.prepareBroadcastMessages(i, parties, storages, retrievers);
    }
    steps = PbctsKeyGenTest.executeRound(retrievers, steps);
    return PbctsKeyGenTest.finalizeProtocols(steps);
  }

  @Test
  void sign() {
    List<ThresholdSignatureProvider> signers = createSigners(4);

    Hash message = Hash.create(s -> s.writeString("sign here"));
    String signatureId = signers.get(0).getSignatureIds().get(0);

    for (int i = 0; i < parties.size(); i++) {
      ThresholdSignatureProvider signer = signers.get(i);
      Assertions.assertThat(signer.getRemainingSignatures()).isEqualTo(5);
      signer.preSignAndStore(message, signatureId, dummyRandomizer);
      Assertions.assertThat(signer.getRemainingSignatures()).isEqualTo(4);
    }

    List<PartialSignatureRetriever> retrievers = getRetrievers(parties);
    preparePartialSignatures(signatureId, retrievers);

    for (int i = 0; i < parties.size(); i++) {
      ThresholdSignatureProvider signer = signers.get(i);
      List<SignatureAndContributors> signatureAndContributors =
          signer.collectNewSignatures(retrievers.get(i));
      Assertions.assertThat(signatureAndContributors).hasSize(1);
      SignatureAndContributors sac = signatureAndContributors.get(0);
      Assertions.assertThat(sac.contributors).containsExactly(0L, 1L, 2L, 3L);
      Assertions.assertThat(sac.signatureId).isEqualTo(signatureId);
      BlockchainPublicKey recovered = sac.signature.recoverPublicKey(message);
      Assertions.assertThat(recovered).isEqualTo(signer.getPublicKey().getPublicKey());

      signatureAndContributors = signer.collectNewSignatures(retrievers.get(i));
      Assertions.assertThat(signatureAndContributors).isEmpty();
      Assertions.assertThat(signer.getRemainingSignatures()).isEqualTo(4);
    }
  }

  private List<PartialSignatureRetriever> getRetrievers(List<BlockchainPublicKey> parties) {
    List<PartialSignatureRetriever> retrievers = new ArrayList<>();
    for (int i = 0; i < parties.size(); i++) {
      retrievers.add(Mockito.mock(PartialSignatureRetriever.class));
    }
    return retrievers;
  }

  @Test
  void unknownSignatureId() {
    List<ThresholdSignatureProvider> signers = createSigners(4);
    Hash message = Hash.create(s -> s.writeString("message"));
    for (int i = 0; i < parties.size(); i++) {
      ThresholdSignatureProvider provider = signers.get(i);
      Assertions.assertThat(provider.getRemainingSignatures()).isEqualTo(5);
      Assertions.assertThatThrownBy(
              () -> provider.preSignAndStore(message, "not_a_signature_id", dummyRandomizer))
          .isInstanceOf(IllegalArgumentException.class)
          .hasMessage("Unknown signature ID: not_a_signature_id");
      Assertions.assertThat(provider.getRemainingSignatures()).isEqualTo(5);
    }
  }

  @Test
  void combine() {
    List<ThresholdSignatureProvider> signers = createSigners(4);
    Hash message = Hash.create(s -> s.writeString("another message"));

    ThresholdSignatureProvider signer0 = signers.get(0);
    String signatureId = signer0.getSignatureIds().get(0);
    for (int i = 0; i < parties.size(); i++) {
      ThresholdSignatureProvider signer = signers.get(i);
      signer.preSignAndStore(message, signatureId, dummyRandomizer);
    }

    PartialSignatureRetriever retriever = getRetrievers(parties).get(0);
    List<SignatureAndContributors> signatureAndContributors =
        signer0.collectNewSignatures(retriever);
    Assertions.assertThat(signatureAndContributors).isEmpty();

    preparePartialSignature(signatureId, retriever, 1);
    signatureAndContributors = signer0.collectNewSignatures(retriever);
    Assertions.assertThat(signatureAndContributors).isEmpty();

    preparePartialSignature(signatureId, retriever, 2);
    signatureAndContributors = signer0.collectNewSignatures(retriever);
    Assertions.assertThat(signatureAndContributors).hasSize(1);
    SignatureAndContributors sac = signatureAndContributors.get(0);
    Assertions.assertThat(sac.contributors).containsExactly(0L, 1L, 2L);
    BlockchainPublicKey recovered = sac.signature.recoverPublicKey(message);
    Assertions.assertThat(recovered).isEqualTo(signer0.getPublicKey().getPublicKey());
  }

  @Test
  void combinationFails() {
    List<ThresholdSignatureProvider> signers = createSigners(4);
    Hash message = Hash.create(s -> s.writeString("foo"));

    ThresholdSignatureProvider signer = signers.get(0);
    String signatureId = signer.getSignatureIds().get(0);
    signer.preSignAndStore(message, signatureId, dummyRandomizer);

    PartialSignatureRetriever retriever = Mockito.mock(PartialSignatureRetriever.class);
    Mockito.when(retriever.retrievePartialSignature(sessionId, signatureId, parties.get(1)))
        .thenReturn(new byte[12]);
    Mockito.when(retriever.retrievePartialSignature(sessionId, signatureId, parties.get(2)))
        .thenReturn(new byte[34]);

    List<SignatureAndContributors> signatureAndContributors =
        signer.collectNewSignatures(retriever);
    Assertions.assertThat(signatureAndContributors).isEmpty();
  }

  @Test
  void combinationThreshold() {
    List<ThresholdSignatureProvider> signers = createSigners(9);
    ThresholdSignatureProvider provider = signers.get(0);
    PbctsSigner signer = (PbctsSigner) provider;
    Assertions.assertThat(signer.combinationThreshold()).isEqualTo(5);
  }

  @Test
  void loadFromDatabase() {
    List<BlockchainPublicKey> parties = PbctsKeyGenTest.createParties(4);
    List<MessageStorage> storages = PbctsKeyGenTest.createStorages(parties);
    String keyId = "I7f6NX3qHgvmVlVks2inPsStqcRP5aF3rioegzikNSw";
    for (int i = 0; i < parties.size(); i++) {
      BlockchainPublicKey party = parties.get(i);
      PbctsSigner signer =
          PbctsSigner.load(
              sessionId,
              keyId,
              parties,
              party,
              storages.get(i),
              LongStream.range(0, parties.size()).boxed().toList(),
              "src/test/resources/data_" + i + ".db",
              "",
              databaseKey);
      Assertions.assertThat(signer.getPublicKey().getPublicKeyId()).isEqualTo(keyId);
      Assertions.assertThat(signer.getSignatureIds()).hasSize(5);
      Assertions.assertThat(signer.getRemainingSignatures()).isEqualTo(5);
    }
  }

  private void preparePartialSignatures(
      String signatureId, List<PartialSignatureRetriever> retrievers) {
    for (int sender = 0; sender < parties.size(); sender++) {
      for (int receiver = 0; receiver < parties.size(); receiver++) {
        if (sender != receiver) {
          preparePartialSignature(signatureId, retrievers.get(receiver), sender);
        }
      }
    }
  }

  private void preparePartialSignature(
      String signatureId, PartialSignatureRetriever retriever, int senderIndex) {
    MessageStorage senderStorage = storages.get(senderIndex);
    BlockchainPublicKey sender = parties.get(senderIndex);
    Mockito.when(
            retriever.retrievePartialSignature(
                Mockito.eq(sessionId), Mockito.eq(signatureId), Mockito.eq(sender)))
        .thenReturn(senderStorage.getPartialSignature(sessionId, signatureId));
  }
}
