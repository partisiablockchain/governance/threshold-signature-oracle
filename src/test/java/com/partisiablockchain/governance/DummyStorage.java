package com.partisiablockchain.governance;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import java.util.HashMap;
import java.util.Map;
import org.assertj.core.api.Assertions;

final class DummyStorage implements MessageStorage {

  private final ThresholdSessionId sessionId;
  private final Map<BlockchainPublicKey, byte[]> roundMessages = new HashMap<>();
  private final Map<Hash, byte[]> broadcastMessages = new HashMap<>();
  private final Map<Integer, Hash> myBroadcastMessages = new HashMap<>();
  private final Map<String, byte[]> partialSignatures = new HashMap<>();

  DummyStorage(ThresholdSessionId sessionId) {
    this.sessionId = sessionId;
  }

  @Override
  public byte[] getPartialSignature(ThresholdSessionId sessionId, String signatureId) {
    Assertions.assertThat(sessionId).isEqualTo(this.sessionId);
    return partialSignatures.get(signatureId);
  }

  @Override
  public void setPartialSignature(ThresholdSessionId sessionId, String signatureId, byte[] data) {
    Assertions.assertThat(sessionId).isEqualTo(this.sessionId);
    Assertions.assertThat(partialSignatures.get(signatureId)).isNull();
    partialSignatures.put(signatureId, data);
  }

  @Override
  public byte[] getRoundUnicastMessage(
      ThresholdSessionId sessionId, int roundNumber, BlockchainPublicKey to) {
    Assertions.assertThat(sessionId).isEqualTo(this.sessionId);
    Assertions.assertThat(roundNumber).isEqualTo(2);
    // rounds are 1 indexed, so make a small adjustment here
    return roundMessages.get(to);
  }

  @Override
  public void setRoundUnicastMessage(
      ThresholdSessionId sessionId, int roundNumber, BlockchainPublicKey to, byte[] data) {
    Assertions.assertThat(sessionId).isEqualTo(this.sessionId);
    Assertions.assertThat(roundNumber).isEqualTo(2);
    // rounds are 1 indexed, so make a small adjustment here
    roundMessages.put(to, data);
  }

  @Override
  public byte[] getBroadcastMessage(Hash messageHash) {
    return broadcastMessages.get(messageHash);
  }

  @Override
  public void setBroadcastMessage(ThresholdSessionId sessionId, int roundNumber, byte[] message) {
    Assertions.assertThat(sessionId).isEqualTo(this.sessionId);
    Hash messageHash = Hash.create(s -> s.write(message));
    myBroadcastMessages.put(roundNumber, messageHash);
    broadcastMessages.putIfAbsent(messageHash, message);
  }

  byte[] getMyBroadcastMessage(int roundNumber) {
    Hash hash = myBroadcastMessages.get(roundNumber);
    return broadcastMessages.get(hash);
  }
}
