package com.partisiablockchain.governance;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import nl.jqno.equalsverifier.EqualsVerifier;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ThresholdSessionIdTest {

  @Test
  void getters() {
    ThresholdSessionId thresholdSessionId = new ThresholdSessionId(123, 456);
    Assertions.assertThat(thresholdSessionId.getSessionId()).isEqualTo(123);
    Assertions.assertThat(thresholdSessionId.getRetryNonce()).isEqualTo(456);
  }

  @Test
  void equalsTest() {
    EqualsVerifier.forClass(ThresholdSessionId.class).verify();
  }

  @Test
  void toStringTest() {
    ThresholdSessionId thresholdSessionId = new ThresholdSessionId(123, 456);
    Assertions.assertThat(thresholdSessionId.toString())
        .isEqualTo("(sessionId=123, retryNonce=456)");
  }
}
