# threshold-signature-oracle

This repository contains code for running and managing one or more
processes for generating and using threshold ECDSA keys, using an
external library written in Golang.

## Building `pbcts.so`

This library requires the presence of a shared library file,
`pbcts.so` that is allows calls to the external threshold signature
library.

The following instructions assumes access to the Go library.

To build `pbcts.so`, it should suffice to run the commands

```
cd go
go build -o pbcts.so -buildmode=c-shared *go
```

In case the above does not work, one can try the following

```
go get gitlab.com/sepior/pbcts-lib
cd go
echo "module some/code" > go.mod
go mod tidy
```

This should create a valid `go.mod` file which would allow building
the shared library file using the `go build` command as described
above.
